"""memoize provides a decorator to memoize functions and methods."""
import functools
import time

def memoized(obj, max_age=None):
    _cache = {}
    _age = {}

    @functools.wraps(obj)
    def memoizer(*args, **kwargs):
        key = str(args) + str(kwargs)
        now = time.time()
        if key not in _cache or (now - _age[key]) > max_age:
            _cache[key] = obj(*args, **kwargs)
            _age[key] = time.time()
        return _cache[key]
    return memoizer
