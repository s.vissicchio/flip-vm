#! /usr/bin/python
# -*- coding: utf-8 -*-

import abc
import copy
import logging
import logging.config
import lplib
import memoize
import networkx as nx
import random
import util
import verify


logging.config.fileConfig('logging.conf')
log = logging.getLogger(__name__)


class PreconditionFailedError(Exception):
    pass


class Graph(object):
    """An object that encapsulates all we know about the graph.

    Externally it behaves like a nx.DiGraph but internally it keeps multiple
    graphs to be able to efficiently find nexthops according to either the
    initial or final FIB.

    Attributes:
        initial  (nx.DiGraph) a graph representing the initial FIB for a
                 given destination
        final    (nx.DiGraph) a graph representing the final FIB for a
                 given destination
        dst      (NodeID) the ID of the destination
        srcs     (list<NodeID>) the list of source nodes that will send traffic
                 to the given destination
        subpaths (list<sequence<NodeID>>) the list of subpaths that traffic to
                 the given destination must pass. At least one of the subpaths
                 must be present in every path from sources to the destination
    """
    def __init__(self, initial, final, dst, srcs=None, subpaths=None):
        self.initial = initial.copy()
        self.final = final.copy()
        self.dst = dst
        if srcs is None:
            srcs = []
        self.srcs = srcs
        if subpaths is None:
            subpaths = []
        self.subpaths = subpaths

    def __str__(self):
        return 'Graph<\nsrcs:%s\ndst:%s\ninitial:%s\nfinal:%s\n>' % (
            self.srcs, self.dst,
            nx.to_dict_of_dicts(self.initial),
            nx.to_dict_of_dicts(self.final))

    def _remove_subpaths(self, dag, subpaths):
        """Removes subpaths from dag."""
        for path in subpaths:
            if len(path) == 1:
                if  dag.has_node(path[0]):
                    for i in dag.successors(path[0]):
                        dag.remove_edge(path[0], i)
            else:
                for i in range(0, len(path) - 1):
                    if dag.has_edge(path[i], path[i + 1]):
                        dag.remove_edge(path[i], path[i + 1])

    def initialize(self):
        """Lazy initialization of this Graph object."""
        self.color_dag(self.initial)
        self.color_dag(self.final)

    def color_dag(self, dag):
        """Color a DAG based on whether nodes satisfy policy constraints.

        Node color depends on the topology:
        1) we consider the subgraph induced by the sources. Every node
           not appearing in this subgraph is yellow.
        2)
          2.1) remove policy subpaths from the subgraph
          2.2) nodes that can reach the destination are white
          2.3) nodes that are reachable from a source are green
          2.4) if any green or white node appears in a policy subpath
               (without being the first or last node, resp.), drop the policy
               subpath and start over at step 2)
        3) all nodes having only green successors (in the original DAG) are
           green
        4) every other node is cyan

        Intuitively, green nodes are safe to use because they guarantee that
        the constraint will be satisfied. White nodes assume that the
        constraint has already been satisfied, so they may be unsafe to use.
        Yellow nodes don't have a path to the destination and are irrelevant.
        Cyan nodes are special in that we need to enumerate all possible
        paths traversing them before we can determine if they are safe or not.

        Args:
            dag      (nx.DiGraph)

        Returns:
            colors the input dag and returns a reference to it.
        """
        greens = set()
        whites = set()
        cyans = set()
        yellows = set()
        allnodes = set(dag.nodes())
        if not self.subpaths:
            for node in allnodes:
                dag.node[node]['color'] = 'green'
            dag.node[self.dst]['color'] = 'white'
            return
        # Create a subgraph containing all and only the nodes that appear
        # on a src->dst path in the original graph.
        complete_src_dst_dag = nx.DiGraph()
        for src in self.srcs:
            for p in nx.all_simple_paths(dag, src, self.dst):
                complete_src_dst_dag.add_path(p)
        # All nodes that are not in the complete subgraph are yellow.
        for node in allnodes - set(complete_src_dst_dag.nodes()):
            log.debug('node %s not reachable from srcs, colored yellow', node)
            dag.node[node]['color'] = 'yellow'
            yellows.add(node)

        found_superfluous_subpaths = True
        normalized_subpaths = self.subpaths[:]
        while found_superfluous_subpaths:
            found_superfluous_subpaths = False
            tmp_dag = complete_src_dst_dag.copy()
            # Remove policy subpaths from the subgraph
            self._remove_subpaths(tmp_dag, normalized_subpaths)
            # All nodes reachable from the destination going backwards are white.
            for node in nx.dfs_preorder_nodes(nx.reverse(tmp_dag), self.dst):
                if node in whites:
                    continue
                log.debug('node %s in dst component, colored white', node)
                dag.node[node]['color'] = 'white'
                whites.add(node)
            # All nodes reachable from a source are green.
            # If a green node appears in a subpath without being the first
            # node, then that subpath is superfluous, so we need to start
            # over by repruning the topology
            for src in self.srcs:
                for node in nx.dfs_preorder_nodes(tmp_dag, src):
                    if node not in greens:
                        log.debug('node %s in same component as src %s, '
                                  'colored green', node, src)
                        dag.node[node]['color'] = 'green'
                        greens.add(node)
                for p in normalized_subpaths[:]:
                    if (set(p[1:]) & greens) or (set(p[:-1]) & whites):
                        log.warn('subpath %s is superfluous, discarding it', p)
                        found_superfluous_subpaths = True
                        normalized_subpaths.remove(p)
        # Enumerate nodes in reverse topological order. A node having
        # only green successors is itself green.
        for node in nx.topological_sort(tmp_dag, reverse=True):
            if node in greens | whites | yellows:
                continue
            if (dag.successors(node) and
                    all([x in greens for x in dag.successors(node)])):
                log.debug('node %s has all-green successors, colored green', node)
                dag.node[node]['color'] = 'green'
                greens.add(node)
        # All nodes in the subgraph that have not been colored yet are cyan.
        for node in set(tmp_dag.nodes()) - (greens | yellows | whites):
            log.debug('node %s colored cyan', node)
            dag.node[node]['color'] = 'cyan'
            cyans.add(node)
        # Verify that the invariants we rely on are satisfied
        desc = 'initial' if dag is self.initial else 'final'
        for node in allnodes:
            if node is self.dst and dag.node[node]['color'] != 'white':
                raise PreconditionFailedError(
                    'destination %s is not white in %s graph' %(self.dst, desc))
            if node in self.srcs and dag.node[node]['color'] != 'green':
                raise PreconditionFailedError(
                    'source %s is not green in %s graph' %(node, desc))
            if 'color' not in dag.node[node]:
                raise PreconditionFailedError(
                    'node %s was not colored in %s graph' %(node,desc))

    def copy(self):
        """Returns a copy of this Graph."""
        other = Graph(initial=self.initial.copy(),
                      final=self.final.copy(),
                      dst=copy.copy(self.dst),
                      srcs=self.srcs[:],
                      subpaths=copy.deepcopy(self.subpaths))
        return other

    @memoize.memoized
    def find_crucial_predecessors(self, node, dag):
        """Finds the crucial predecessors in the given dag for a node.

        A set of crucial predecessors is a set u_0,..,u_k of nodes such that
          1) u_i are exclusive predecessors of the node
          2) u_i are both in the initial and in the final graph
          3) there is no path from any source to the node that doesn't
             traverse at least one u_i

        We visit the graph with a reverse DFS starting from node, and pick up
        all (transitive) predecessors that are predecessors in this DAG but not
        in the other, skipping the nodes that are missing in the other DAG.

        Args:
            node (NodeID) the ID of the node whose crucial predecessors we
                 want to find
            dag  (nx.DiGraph) the graph in which we want to search. If not
                 provided we search on both DAGs.

        Returns:
            a pair (set<NodeID>, set<NodeID>). The first set is the set
            of crucial predecessors of the input node. The second set is a
            subset of the first set and contains the non-leaf crucial
            predecessors, i.e., those crucial predecessors that have other
            predecessors that are in turn crucial.
        """
        # NOTE: the implemented selection of crucial predecessors is likely not to be optimal. Indeed, it implements a heuristic that explores all the branches from the input node backwards to the sources and selects the first crucial predecessor on each of those branches. Starting from the sources may result in a smaller number of crucial predecessors. A further alternative to this procedure is to use graph separators on the subgraph from all sources to the given node.
        crucials = set()
        stack = [node]
        visited = set()
        non_leaf = set()
        # dags_to_consider is a list of triples (dag, other dag, name)
        dags_to_consider = [(self.initial, self.final, 'initial'),
                            (self.final, self.initial, 'final')]
        if dag is not None:
            if dag is self.initial:
                dags_to_consider.pop(-1)
            elif dag is self.final:
                dags_to_consider.pop(0)
            else:
                raise PreconditionFailedError('unknown dag')
        while stack:
            n = stack.pop()
            if n in self.srcs and n != node:
                crucials.add(n)
                continue
            visited.add(n)
            if n in crucials:
                # We marked this node as crucial during another visit
                # (from another branch). In this branch though, we're
                # expanding its predecessors, hence it is a non-leaf.
                non_leaf.add(n)
                log.log(5, 'node %s has non-leaf crucial predecessor %s',
                        node, n)
            for dag, other_dag, desc in dags_to_consider:
                if not dag.has_node(n):
                    continue
                if not dag.predecessors(n) and n != node:
                    log.log(5, 'looking for crucial predecessors for '
                               '%s we reached %s (%s-%s), which has no '
                               'predecessors and is not a source',
                               node, n, self.initial.node[n]['color'],
                               self.final.node[n]['color'])
                    if dag.node[n]['color'] != 'yellow':
                        raise PreconditionFailedError(
                            'node %s (%s-%s) has no predecessors in %s DAG '
                            'crucial predecessors for %s cannot be computed' %(
                                n, self.initial.node[n]['color'],
                                self.final.node[n]['color'], desc, node))
                for pred in dag.predecessors(n):
                    if (
                            other_dag.has_edge(pred, n) or
                            not other_dag.has_node(pred)):
                        stack.append(pred)
                    else:
                        if pred in visited:
                            # We already skipped through this node but
                            # this time we find it's crucial, hence
                            # it is a non-leaf
                            log.log(5, 'node %s has non-leaf crucial '
                                    'predecessor %s', node, pred)
                            non_leaf.add(pred)
                        crucials.add(pred)
        return crucials, non_leaf


######################
# Constraint classes #
######################
class UpdateConstraint(object):
    """A constraint to update a set of routers.

    Instances of this class must be immutable.
    """
    __metaclass__ = abc.ABCMeta

    def __cmp__(self, other):
        return cmp(str(self), str(other))

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return str(self)

    @abc.abstractmethod
    def __str__(self): pass


class OrderingConstraint(UpdateConstraint, lplib.ConvertibleToILPConstraint):
    __metaclass__ = abc.ABCMeta

    def __init__(self, before_grp, after_grp):
        self.before_grp = before_grp
        self.before_grp.sort()
        self.after_grp = after_grp
        self.after_grp.sort()

    def __str__(self):
        return '%s: %s %s < %s' % (
            self.family, self.kind,
            sorted(self.before_grp), sorted(self.after_grp))

    @abc.abstractproperty
    def family(self): pass

    @abc.abstractproperty
    def kind(self): pass

    def get_nodes(self):
        return self.before_grp | self.after_grp

    @abc.abstractmethod
    def replace(self, old, new_list):
        """Creates new constraints that replace a node in an existing one.

        This constraint is unmodified because constraints are immutable.
        The returned constraints are semantically equivalent to the input
        constraint, except that the occurrences of the old nodeID is replaced
        with the substitutes in new_list.

        Args:
          old, nodeID, the node ID to be replaced
          new_list, set<nodeID>, list of node IDs to replace 'old' with

        Returns:
          list<OrderingConstraint> (empty if this constraint doesn't reference
          the 'old' nodeID).
        """

    @abc.abstractmethod
    def to_list(self): pass


class AnyOrderingConstraint(OrderingConstraint):
    @property
    def kind(self): return 'ANY'

    def replace(self, old, new_list):
        """See OrderingConstraint.replace."""
        replacement = []
        for grp in (self.before_grp, self.after_grp):
            if old in grp:
                for node in new_list:
                    new = copy.deepcopy(self)
                    if grp is self.before_grp:
                        modified_grp = new.before_grp
                    else:
                        modified_grp = new.after_grp
                    modified_grp.remove(old)
                    modified_grp.append(node)
                    replacement.append(new)
        return replacement

    def to_list(self):
        if self.before_grp == self.after_grp and len(self.before_grp) <= 1:
            return []
        return [(self.before_grp, self.after_grp)]


class AllOrderingConstraint(OrderingConstraint):
    @property
    def kind(self): return 'ALL'

    def replace(self, old, new_list):
        """See OrderingConstraint.replace."""
        replacement = []
        modified_grp = None
        for grp in (self.before_grp, self.after_grp):
            if old in grp:
                if not replacement:
                    # Lazily initialize the replacement constraint
                    replacement.append(copy.deepcopy(self))
                if grp is self.before_grp:
                    modified_grp = replacement[0].before_grp
                else:
                    modified_grp = replacement[0].after_grp
                modified_grp.remove(old)
                modified_grp.extend(new_list)
        return replacement

    def to_list(self):
        return [([b], [a]) for b in self.before_grp
                           for a in self.after_grp
                           if a != b]


class BlackholeConstraint(AllOrderingConstraint):
    @property
    def family(self): return 'BLACKHOLE'


class LoopConstraint(AnyOrderingConstraint):
    @property
    def family(self): return 'LOOP'


class PolicyConstraint(AllOrderingConstraint):
    @property
    def family(self): return 'POLICY'


class AbstractMatchingConstraint(UpdateConstraint):
    """An abstract base class for matching constraints."""
    @abc.abstractproperty
    def family(self): pass

    @abc.abstractproperty
    def kind(self): pass

    @abc.abstractmethod
    def get_taggers(self): pass


class MatchingConstraint(AbstractMatchingConstraint):
    """A matching constraint that represents duplicating the FIB at a node.

    Attributes:
        node       (NodeID) the ID of the node whose FIB we duplicate
        tag_initial (set<NodeID>) the nodes that will set the tag in the
                    initial network configuration
        tag_final (set<NodeID>) the nodes that will set the tag in the
                  final network configuration
    """
    KIND = 'MATCH'

    def  __init__(self, node=None, tag_initial=None, tag_final=None):
        self.node = node
        self.tag_initial = tag_initial or set([])
        self.tag_final = tag_final or set([])

    def __str__(self):
        return '%s: on %s' % (self.KIND, self.node)

    @property
    def family(self): return ''

    @property
    def kind(self): return 'MATCH'

    def get_taggers(self): return self.tag_initial | self.tag_final


# TODO: is it possible to improve the way constraints are registered and chosen?
class ConstraintStorage(object):
    """A storage for constraints deriving from a Graph.

    Each constraint is a subclass of UpdateConstraints.

    Constraints are organized in groups, in such a way that a single constraint
    belongs to a single group. A ConstraintStorage object supports adding
    alternative constraints for an existing constraint.
    Newly added OrderingConstraints are automatically marked as active.

    Attributes:
      active_constraints: set<constraint>, the set of currently active
                          constraints in this storage.
      alternative_constraints: dict<constraint, list<constraint>> maps a
                               constraint to its alternatives
      constraints2groups: dict<constraint, group>, maps a constraint to a group
      dangerous_subpaths: list<list<nodeID>>, the list of subpaths that are
                          potentially at risk of creating a policy violation.
      dependencies: dict<constraint, list<constraint>>, maps a constraint to
                    a list of constraints that it depends on.
      duplicate: bool, whether or not we should duplicate on cyan-cyan nodes.
      graph: Graph
      groups2constraints: dict<group, list<constraint>>, the reverse mapping
      initial_swap: list<constraint> a list of constraint to swap immediately
                    at initialization.
      optimization_strategy: an object implementing select_best_alternatives()
                             It is called to pick constraints and alternatives.
    """

    _REGISTRY = {   # shorthand for group names based on constraints
        BlackholeConstraint: 'B',
        LoopConstraint: 'L',
        PolicyConstraint: 'P',
        MatchingConstraint: 'M',
    }

    def __init__(self, graph=None, optimization_strategy=None, duplicate=False):
        self._initialized = False
        self.active_constraints = set()
        self.alternative_constraints = {}
        self.constraints2groups = {}
        self.dangerous_subpaths = []
        self.dependencies = {}
        self.duplicate = duplicate
        self.graph = graph
        self.groups2constraints = {}
        self.initial_swap = []
        self.optimization_strategy = optimization_strategy

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.to_string()

    def _add_alternative_constraints(self, con, list_alternatives):
        """Maps a constraint to a list of alternative constraints."""
        if not list_alternatives:
            log.warn('constraint %s has empty list of alternatives', con)
            return
        self._add_to_alternative_dict(con, list_alternatives)
        for alt in list_alternatives:
            self._add_to_alternative_dict(alt, [con])

    def _add_blackhole_constraints(self, node):
        """Generate constraints to avoid blackholes.

        We are only interested in nodes that are non-present in one
        of the DAGs.

        Args:
            node       (NodeID) the node to consider
        """
        if (
                (self.graph.initial.has_node(node) and not
                 self.graph.final.has_node(node)) or
                (self.graph.final.has_node(node) and not
                 self.graph.initial.has_node(node))):
            graph = (self.graph.initial if self.graph.initial.has_node(node)
                                       else self.graph.final)
            # If node is a blackhole we want it migrated either before or
            # after its crucial predecessors.
            crucial_predecessors = list(
                self.graph.find_crucial_predecessors(node, graph)[0])
            if not crucial_predecessors:
                log.debug('node %s is a blackhole but it is unreachable from '
                          'any source, so we skip adding the constraint', node)
                return
            # Emit a constraint to force
            # (1) initially-used nodes to be updated (removed) last, and
            # (2) finally-used nodes to be updated (added) first.
            aft_grp = []
            bfr_grp = []
            if self._has_color(node, 'yellow', '*', True, False):
                aft_grp = crucial_predecessors
                bfr_grp = [node]
            elif self._has_color(node, '*', 'yellow', False, True):
                bfr_grp = crucial_predecessors
                aft_grp = [node]
            else:
                raise RuntimeError(
                    'Unexpected blackhole constraint for node %s' % node)
            c = BlackholeConstraint(before_grp=bfr_grp, after_grp=aft_grp)
            log.info('node %s adds %s', node, c)
            self._add_constraint(c, 'B')

    def _add_constraint(self, con, grp):
        assert grp == self._REGISTRY[type(con)], 'unknown group %s' % grp
        self.groups2constraints.setdefault(grp, set()).add(con)
        self.constraints2groups[con] = grp

    def _add_constraints(self, conlist, grp):
        for c in conlist:
            self._add_constraint(c, grp)

    def _add_cycle_constraints(self):
        """Generate constraints to avoid cycles.

        Enumerate all simple cycles that are reachable from at least one source
        and for each of them add constraints.
        """
        temp_dag = nx.compose(self.graph.initial.copy(),
                              self.graph.final.copy())
        for node in temp_dag.nodes():
            if not any([nx.has_path(temp_dag, s, node)
                        for s in self.graph.srcs]):
                temp_dag.remove_node(node)
        occurrences_in_cycles = {}  # node --> list<cycles> where it appears
        for cycle in nx.simple_cycles(temp_dag):
            # initialization
            mcons = []
            init_grp = []
            fin_grp = []
            init_tag = set()
            fin_tag = set()
            dep_init_tag = set()
            dep_fin_tag = set()
            for i in range(len(cycle)):
                # identify one node and its neighbors in the cycle.
                curr = cycle[i]
                nxthop = cycle[(i + 1) % len(cycle)]
                occurrences_in_cycles.setdefault(curr, []).append(cycle)
                # first check if the current node can break the cycle
                if (self.graph.initial.has_edge(curr, nxthop) and
                         self.graph.final.has_edge(curr, nxthop)):
                    continue
                if self.graph.initial.has_edge(curr, nxthop):
                    init_grp.append(curr)
                elif self.graph.final.has_edge(curr, nxthop):
                    fin_grp.append(curr)
                fin_tag, dep_fin_tag = self.graph.find_crucial_predecessors(
                    curr, self.graph.final)
                init_tag, dep_init_tag = self.graph.find_crucial_predecessors(
                    curr, self.graph.initial)
                # we can only generate a tag-and-match constraint if we're able
                # to find crucial predecessors.
                if init_tag or fin_tag:
                    m = MatchingConstraint(curr, init_tag, fin_tag)
                    mcons.append(m)
                    self._add_constraint(m, 'M')
                    deps = []
                    for dep in dep_fin_tag | dep_init_tag:
                        mdep = MatchingConstraint(
                            dep,
                            self.graph.find_crucial_predecessors(
                                dep, self.graph.initial)[0],
                            self.graph.find_crucial_predecessors(
                                dep, self.graph.final)[0])
                        if mdep.tag_initial or mdep.tag_final:
                            self._add_constraint(mdep, 'M')
                            deps.append(mdep)
                    if deps:
                        self._add_dependencies(m, deps)

            # generate an ordering constraint
            ordcon = LoopConstraint(init_grp, fin_grp)
            log.info('node %s (%s->%s) adds %s',
                     cycle[0], '->'.join(cycle), cycle[0], ordcon)
            self._add_constraint(ordcon, 'L')
            # Set previously-generated matching constraints as alternatives:
            # if any node in the cycle matches its initial and final forwarding
            # state, then infinite loops are avoided.
            self._add_alternative_constraints(ordcon, mcons)
        for node, cycles in occurrences_in_cycles.items():
            if len(cycles) > 1:
                # We add a dependency where matching on this node implies
                # matching on all nodes in the cycles it appears in as well.
                con = MatchingConstraint(
                    node,
                    tag_initial=self.graph.find_crucial_predecessors(
                            node, self.graph.initial)[0],
                    tag_final=self.graph.find_crucial_predecessors(
                            node, self.graph.final)[0])
                deps = set()
                for cycle in cycles:
                    for other_node in cycle:
                        m = MatchingConstraint(
                            other_node,
                            tag_initial=self.graph.find_crucial_predecessors(
                                other_node, self.graph.initial)[0],
                            tag_final=self.graph.find_crucial_predecessors(
                                other_node, self.graph.final)[0])
                        self._add_constraint(m, 'M')
                        deps.add(m)
                log.debug('node %s appears in multiple cycles %s, '
                          'adding dependency %s => %s',
                          node, cycles, con, deps)
                self._add_dependencies(con, deps)

    def _add_dependencies(self, constraint, deps):
        """Store the dependencies for the given constraint."""
        self.dependencies.setdefault(constraint, [])
        self.dependencies[constraint].extend(deps)

    def _add_policy_constraints(self, node):
        """Generate constraints to satisfy policies.

        Constraints are generated based on the following matrix
        (I = initial DAG, F = final DAG)

         ` F     G(reen)    |     C(yan)     |   W(hite)/yellow
        I `-----------------+----------------+-----------
        G |        -        | n > pred(n, I) | n > pred(n, I)
        C |  n < pred(n, F) |    see NOTE    | n > pred(n, I)
        W |  n < pred(n, F) | n < pred(n, F) |      -        

        NOTE: for cyan-cyan nodes, we need to consider all possible paths that
        traverse them. If there exists a path P=(u .. v n w .. z) such that
            (i) u is GG,
            (ii) n is CC,
            (iii) z is WW, and
            (iv) P is not compliant with the policy
        then we need to add a constraint.
        The constraint is n < v if (n, w) is in the Initial DAG, otherwise
        it is w < n. The constraint is not generated if [v, n, w] exists in
        either the initial or the final graph.

        Note that this is very conservative, as we are not investigating
        whether the policy constraint might be satisfied by an alternative path:
        we impose that the constraint be satisfied using the same path.

        We also generate an alternative matching constraint at the node.
        Intuitively, we can tag packets before they arrive to the red node so that
        it doesn't inadvertently deflect them towards a non-compliant path.

        Args:
            node     (NodeID) the current node
        """
        # TODO: this function modifies the graph and forces us to keep a
        # reference to the graph in the ConstraintStorage object. Find a way
        # to compute this locally and remove self.graph
        if (
                not self.graph.subpaths or
                not self.graph.initial.has_node(node) or
                not self.graph.final.has_node(node) or
                (self.graph.initial.successors(node) ==
                 self.graph.final.successors(node))):
            return
        if (
                self._has_color(node, 'green', 'green') or
                self._has_color(node, 'white', 'white') or
                self._has_color(node, 'white', 'yellow') or
                self._has_color(node, 'yellow', 'white') or
                self._has_color(node, 'yellow', 'yellow')):
            return
        constraints = set()
        bfr_grp = []
        aft_grp = []
        deps = []
        if self._has_color(node, 'cyan', 'cyan'):
            for p in [x for x in self.dangerous_subpaths if node in x]:
                log.log(5, 'analyzing dangerous path %s for node %s', p, node)
                idx = p.index(node)
                if p.count(node) > 1:
                    # This dangerous path has a cycle containing this node.
                    # Cycles can only arise when tagging and matching, hence
                    # we don't generate an ordering constraint here.
                    # Instead, we make sure every subsequent node matches
                    # until we're out of the loop
                    dag = None
                    if (
                        self.graph.initial.has_edge(p[idx], p[idx+1]) and not
                        self.graph.final.has_edge(p[idx], p[idx+1])):
                        dag = self.graph.initial
                    elif (
                        self.graph.final.has_edge(p[idx], p[idx+1]) and not
                        self.graph.initial.has_edge(p[idx], p[idx+1])):
                        dag = self.graph.final
                    else:
                        # both initial and final DAG have this edge, there is
                        # no point matching at all, skip this dangerous path.
                        continue
                    components = []
                    looping = True
                    i = idx
                    while looping:
                        looping = dag.has_edge(p[i], p[i+1])
                        con = MatchingConstraint()
                        con.node = p[i]
                        con.tag_initial = self.graph.find_crucial_predecessors(
                            con.node, self.graph.initial)[0]
                        con.tag_final = self.graph.find_crucial_predecessors(
                            con.node, self.graph.final)[0]
                        components.append(con)
                        i += 1
                        self._add_constraint(con, 'M')
                    log.debug('cycle %s generated %s', p, components)
                    self._add_dependencies(components[0], components[1:])
                    continue
                if (
                        self.graph.initial.has_edge(p[idx-1], node) and not
                        self.graph.initial.has_edge(node, p[idx+1])):
                    aft_grp = [node]
                    bfr_grp, deps = self.graph.find_crucial_predecessors(
                            node, self.graph.initial)
                    bfr_grp = list(bfr_grp)
                    if bfr_grp and aft_grp:
                        c = PolicyConstraint(bfr_grp, aft_grp)
                        log.log(9, 'node %s adding %s for dangerous path %s',
                                node, c, p)
                        constraints.add(c)
                elif (
                        self.graph.final.has_edge(p[idx-1], node) and not
                        self.graph.final.has_edge(node, p[idx+1])):
                    bfr_grp = [node]
                    aft_grp, deps = self.graph.find_crucial_predecessors(
                        node, self.graph.final)
                    aft_grp = list(aft_grp)
                    if bfr_grp and aft_grp:
                        c = PolicyConstraint(bfr_grp, aft_grp)
                        log.log(9, 'node %s adding %s for dangerous path %s',
                                node, c, p)
                        constraints.add(c)
        elif (
                self._has_color(node, 'green', 'cyan') or
                self._has_color(node, 'green', 'white') or
                self._has_color(node, 'green', 'yellow') or
                self._has_color(node, 'cyan', 'white') or
                self._has_color(node, 'cyan', 'yellow')):
            aft_grp = [node]
            bfr_grp = list(self.graph.find_crucial_predecessors(
                    node, self.graph.initial)[0])
            if bfr_grp and aft_grp:
                c = PolicyConstraint(bfr_grp, aft_grp)
                constraints.add(c)
        elif (
                self._has_color(node, 'cyan', 'green') or
                self._has_color(node, 'white', 'green') or
                self._has_color(node, 'yellow', 'green') or
                self._has_color(node, 'white', 'cyan') or
                self._has_color(node, 'yellow', 'cyan')):
            bfr_grp = [node]
            aft_grp = list(self.graph.find_crucial_predecessors(
                    node, self.graph.final)[0])
            if bfr_grp and aft_grp:
                c = PolicyConstraint(bfr_grp, aft_grp)
                constraints.add(c)
        else:
            raise RuntimeError(
                'Node %s has unknown colors: %s, %s' % (node,
                    self.graph.initial.node[node]['color'],
                    self.graph.final.node[node]['color']))
        # We need this check as cyan-cyan might have generated no constraints.
        if not constraints:
            return
        if len(constraints) > 3:
            raise RuntimeError('Node %s yields >3 constraints: %s' % (
                node, constraints))
        # We generate an alternative matching constraint, where we double
        # the state. We do not impose additional specifications on the
        # constraint so that they can be compressed afterwards.
        # E.g., the same tag can be used (by different tagging nodes) to
        # match the initial state of the same node (for multiple destinations).
        for con in constraints:
            log.info(
                'node %s (%s-%s) adds %s with dependencies %s', node,
                 self.graph.initial.node.get(node, {'color': 'none'})['color'],
                 self.graph.final.node.get(node, {'color': 'none'})['color'],
                 con, deps)
            self._add_constraint(con, 'P')
            m = MatchingConstraint()
            m.node = node
            m.tag_initial = self.graph.find_crucial_predecessors(
                node, self.graph.initial)[0]
            m.tag_final = self.graph.find_crucial_predecessors(
                node, self.graph.final)[0]
            self._add_constraint(m, 'M')
            self._add_alternative_constraints(con, [m])
            mdeps = []
            for d in deps:
                m2 = MatchingConstraint(d)
                m2.tag_initial = self.graph.find_crucial_predecessors(
                    d, self.graph.initial)[0]
                m2.tag_final = self.graph.find_crucial_predecessors(
                    d, self.graph.final)[0]
                if m2.tag_initial or m2.tag_final:
                    mdeps.append(m2)
                    self._add_constraint(m2, 'M')
            if mdeps:
                self._add_dependencies(m, mdeps)

    def _add_to_alternative_dict(self, constr, valuelist):
        self.alternative_constraints.setdefault(constr, []).extend(valuelist)

    def _has_color(self, node, col1, col2, missing1=False, missing2=False):
        """Returns true if node has col1 in initial and col2 in final.

        missing1 implies that if the node doesn't exist in the initial
        graph we assume the color is OK (and similarly for missing2).

        Special color '*' acts as a wildcard.
        """
        initial_ok = False
        final_ok = False
        if not self.graph.initial.has_node(node):
            initial_ok =  missing1
        else:
            initial_ok = self.graph.initial.node[node]['color'] == col1
            if col1 == '*':
                initial_ok = True
        if not self.graph.final.has_node(node):
            final_ok = missing2
        else:
            final_ok = self.graph.final.node[node]['color'] == col2
            if col2 == '*':
                final_ok = True
        return initial_ok and final_ok

    def _initialize_active_constraints(self):
        self._initialized = True
        for c in self.constraints2groups.keys():
            if isinstance(c, OrderingConstraint):
                self.active_constraints.add(c)
        for c in self.initial_swap:
            self.swap_with_alternative_constraint(c)

    def _recursively_add_constraints(self, node, currpath, visited):
        """Recursively add constraints to the storage for the current node.

        Add the constraints for the current node, then recurse in a DFS
        to its predecessors.

        Args:
            node     (NodeID) the current node
            currpath (list<NodeID>) the subpath we built so far. This is an
                     accumulator variable used to detect cycles
            visited  (set<NodeID>) the set of nodes we already visited
        """
        if node in visited:
            return
        self._add_blackhole_constraints(node)
        self._add_policy_constraints(node)
        visited.add(node)
        new_currpath = list(currpath)
        new_currpath.append(node)
        for dag in (self.graph.initial, self.graph.final):
            if dag.has_node(node):
                for pred in dag.predecessors(node):
                    self._recursively_add_constraints(pred, new_currpath, visited)

    def copy(self):
        other = ConstraintStorage()
        other.active_constraints = self.active_constraints.copy()
        other.alternative_constraints = self.alternative_constraints.copy()
        other.constraints2groups = self.constraints2groups.copy()
        other.dangerous_subpaths = self.dangerous_subpaths[:]
        other.dependencies = self.dependencies.copy()
        other.duplicate = self.duplicate
        other.graph = self.graph.copy()
        other.groups2constraints = self.groups2constraints.copy()
        other.initial_swap = self.initial_swap[:]
        other.optimization_strategy = self.optimization_strategy
        return other

    def enumerate_dangerous_paths(self, graph):
        """Collect all paths including a cyan-cyan node that violate policy.

        Perform a visit from all cyan-cyan nodes until we reach green-green
        or white-white.

        Args:
          graph: Graph, an initialized Graph object

        Returns:
            a list of dangerous subpaths involving cyan-cyan nodes.
        """
        cyans_i = set([x for x in graph.initial.nodes()
                       if graph.initial.node[x]['color'] == 'cyan'])
        cyans_f = set([x for x in graph.final.nodes()
                       if graph.final.node[x]['color'] == 'cyan'])
        cyans = cyans_i & cyans_f
        if not cyans:
            log.debug('no cyan-cyan nodes, skipping path enumeration')
            return []
        dangerous = []
        merged_dag = nx.compose(graph.initial, graph.final)
        # nx.compose doesn't compose the colors, so do it manually
        color = {}
        for n in merged_dag.nodes():
            col1 = col2 = None
            if graph.initial.has_node(n):
                col1 = graph.initial.node[n]['color']
            if graph.final.has_node(n):
                col2 = graph.final.node[n]['color']
            color[n] = '%s-%s' % (col1, col2)
        greens = util.enumerate_frontier(
            nx.reverse(merged_dag.copy()), cyans,
            lambda n: color[n] == 'green-green')
        whites = util.enumerate_frontier(
                merged_dag.copy(), cyans,
                lambda n: color[n] == 'white-white')
        if not greens or not whites:
            log.critical('Colors: %s, Srcs: %s, Dst: %s',
                dict([(x, color[x]) for x in merged_dag.nodes()]),
                graph.srcs, graph.dst)
            raise RuntimeError('No green-green or white-white node found')
        if self.duplicate:
            for n in set(merged_dag.nodes()) - greens - whites:
                if (self._has_color(n, 'cyan', '*') or
                    self._has_color(n, '*', 'cyan')):
                    m = MatchingConstraint()
                    m.node = n
                    m.tag_initial = self.graph.find_crucial_predecessors(
                        n, self.graph.initial)[0]
                    m.tag_final = self.graph.find_crucial_predecessors(
                        n, self.graph.final)[0]
                    self._add_constraint(m, 'M')
                    self.initial_swap.append(m)
            return []
        # Add a dummy src and dst node
        s = '__src__'
        d = '__dst__'
        for g in greens:
            merged_dag.add_edge(s, g)
        for w in whites:
            merged_dag.add_edge(w, d)
        dangerous = [p[1:-1]
            for p in util.all_paths_with_cycles(merged_dag, s, d)
            if set(p) & cyans]
        dangerous = [p for p in dangerous
                       if not self.is_compliant(p, graph.subpaths)]
        if dangerous:
            log.debug('paths %s\n\nare in violation of policy %s',
                      dangerous, graph.subpaths)
        return dangerous
        
    def extract(self):
        """Adds constraints from the underlying Graph."""
        self.graph.initialize()
        self.dangerous_subpaths = self.enumerate_dangerous_paths(self.graph)
        self._add_cycle_constraints()
        self._recursively_add_constraints(
            self.graph.dst, currpath=[], visited=set())
        self._initialize_active_constraints()

    def get_alternatives(self, con):
        return self.alternative_constraints.get(con, [])

    def get_group(self, grp, also_inactive=False):
        sieve = lambda c: c in self.active_constraints
        if also_inactive:
            sieve = lambda c: True
        return set([c for c in self.groups2constraints.get(grp, [])
                      if sieve(c)])

    def get_active_constraints(self):
        """Returns the currently active constraints.

        If the storage has not been initialized yet, active constraints
        are initialized to ordering constraints.
        """
        if not self._initialized:
            self._initialize_active_constraints()
        return self.active_constraints

    def get_blackhole_constraints(self):
        return self.get_group(self._REGISTRY[BlackholeConstraint])

    def get_loop_constraints(self):
        return self.get_group(self._REGISTRY[LoopConstraint])

    def get_matching_constraints(self):
        return self.get_group(self._REGISTRY[MatchingConstraint])

    def get_policy_constraints(self):
        return self.get_group(self._REGISTRY[PolicyConstraint])

    def get_variables(self):
        return set(self.graph.initial.nodes()) | set(self.graph.final.nodes())

    @classmethod
    def is_compliant(cls, path, subpaths):
        compliant = not subpaths
        for subpath in subpaths:
            try:
                start = path.index(subpath[0])
                end = path.index(subpath[-1])
            except ValueError:
                continue
            if path[start:end+1] == subpath:
                compliant = True
                break
        return compliant
            
    def replace_constraint(self, old, news):
        """Replace an old constraint with a list of new constraints."""
        if not news or not old:
            raise PreconditionFailedError(
                'called replace_constraint with empty or None argument: '
                '%s, %s' % (news, old))
        group = self.constraints2groups[old]              # must exist
        del self.constraints2groups[old]
        self.groups2constraints[group].remove(old)        # must exist
        self.active_constraints.remove(old)               # must be active
        alternatives = self.get_alternatives(old)
        if alternatives:
            del self.alternative_constraints[old]
            for alt in alternatives:
                self.alternative_constraints[alt].remove(old)  # must exist
        for new in news:
            if type(new) is not type(old):
                raise PreconditionFailedError(
                    'cannot replace constraint %s with %s' % (old, new))
            self.active_constraints.add(new)
            self.constraints2groups[new] = group
            self.groups2constraints[group].add(new)
            for alt in alternatives:
                self.alternative_constraints[alt].append(new)
            self.alternative_constraints[new] = alternatives
        
    def select_best_alternatives(self):
        """Return a new, optimized ConstraintStorage object.

        The new object is built by selecting the "best" alternatives from the
        constraints of this object.

        Returns:
            a new ConstraintStorage object
        """
        if self.optimization_strategy is None:
            raise NotImplementedError('cannot optimize without a strategy')
        return self.optimization_strategy.select_best_alternatives(self)

    def swap(self, con):
        """Swap a constraint with its alternatives recursively.

        Enumerate all possible dependencies of the constraint and swap them
        all in.

        Args:
            con: MatchingConstraint, the constraint we want to swap in
                 recursively.
        """
        visited = set()
        stack = [con]
        while stack:
            c = stack.pop()
            visited.add(c)
            stack.extend(dep for dep in self.dependencies.get(c, [])
                             if dep not in visited)
        log.debug('swapping in %s and its dependencies %s', con, visited)
        return all(self.swap_with_alternative_constraint(c) for c in visited)

    def swap_with_alternative_constraint(self, con):
        """Swap this constraint with its alternatives.

        The input MatchingConstraint is marked active and all its alternatives
        are removed from the active set.
        We also need to propagate constraints from the matching node to
        its taggers.

        Args:
          con:   MatchingConstraint, the constraint to swap in

        Returns:
          True if the swap was successful, False otherwise.
        """
        if not isinstance(con, MatchingConstraint):
            raise PreconditionFailedError(
                'tried to swap %s which is not a matching constraint' % con)
        for alt in self.alternative_constraints.get(con, []):
            if alt in self.active_constraints:
                self.active_constraints.remove(alt)
        active_matching_constraints = (
            self.get_matching_constraints() & self.get_active_constraints())
        replacement = set(con.tag_initial) | set(con.tag_final)
        # Source nodes are special. They can migrate on their own as well as
        # as a consequence of an upstream node changing the tag.
        # If this matching constraint has a source has a matcher, we need to
        # consider both cases.
        if con.node in self.graph.srcs:
            replacement.add(con.node)
        replaced = set()
        while True:
            to_expand = set(m for m in active_matching_constraints
                               if (m.node in replacement and
                                   m.node not in replaced))
            if not to_expand:
                break
            for c in to_expand:
                log.debug('Recursively expanding %s in %s because of active '
                          'constraint %s', c.node, con, c)
                # Remove c.node *AFTER* adding the replacement, to account for
                # constraints where c.node appears as a tagger.
                replacement |= set(c.tag_initial) | set(c.tag_final)
                # Sources are special, see above.
                if c.node not in self.graph.srcs:
                    replacement.remove(c.node)
                replaced.add(c.node)
        # A small optimization: we know that MatchingConstraints can only
        # be swapped in and OrderingConstraints can only be swapped out,
        # so we only look for the latter.
        for c in (self.get_blackhole_constraints() |
                  self.get_policy_constraints() |
                  self.get_loop_constraints()):
            new_cons = c.replace(con.node, replacement)
            if new_cons:
                log.debug('Replacing %s --> %s', c, new_cons)
                self.replace_constraint(c, new_cons)
        self.active_constraints.add(con)
        return True

    def to_string(self):
        desc = ['*** currently active constraints ***']
        for constr in self.active_constraints:
            desc.append('%s' % constr)
        desc.append('  * alternatives *')
        for alt, constr in self.alternative_constraints.iteritems():
            desc.append('%s = %s' %(alt, constr))
        return '\n'.join(desc)


class NaiveMinimizationStrategy(object):
    def pick_alternative(self, storage, constraint):
        """Selects one of the alternatives for a constraint.

        Args:
            storage    (ConstraintStorage) the storage where we get all possible
                       alternatives
            constraint the constraint we want to pick an alternative for

        Returns:
            an alternative constraints if one exists, or None otherwise.
        """
        alts = storage.get_alternatives(constraint)
        if not alts:
            return None
        return alts[0]

    def pick_infeasible_constraint(self, infeasible_constraints):
        """Selects one infeasible constraint from a set."""
        return random.choice(infeasible_constraints)
        
    def select_best_alternatives(self, storage, cutoff=30):
        """Find the minimal set of matching constraints.

        We find the combination of matching constraints and ordering constraints
        that makes the problem feasible and output the selection that brought
        us to the solution.

        We start with an empty set of matching constraints and grow it with
        alternative ways to satisfy one of the constraints in the IIS (Irreducible
        Infeasible Set) of the problem.

        Args:
            storage, ConstraintStorage, the object holding the constraints
            cutoff, int, the maximum number of alternatives we are willing
                    to swap in before declaring the problem too hard.

        Returns:
            a new ConstraintStorage object with the selection of alternatives that
            makes the problem solvable.
        """
        new_storage = storage.copy()
        iis = ['dummy']
        iteration = 0
        while iis and iteration < cutoff:
            iteration += 1
            active_constraints = new_storage.get_active_constraints()
            # if no constraints are left, we're done
            if not active_constraints:
                break
            # compute the set (IIS) of independent infeasible constraints 
            solver = lplib.Solver(active_constraints,
                                  new_storage.get_variables())
            iis = solver.compute_IIS()
            # otherwise, select an alternative for one constraint in the IIS
            alternative = False
            while not alternative and iis:
                torem = self.pick_infeasible_constraint(iis)
                mcon = self.pick_alternative(new_storage, torem)
                if not mcon:
                    iis.remove(torem)
                    continue
                log.debug('trying to swap %s with %s', torem, mcon)
                alternative = new_storage.swap(mcon)
        if iteration == cutoff:
            return None
        return new_storage


##############################
# Rule and Operation classes #
##############################
class FibRule(object):
    def __init__(self,node,match,action):
        self.node = node
        self.match = match
        self.action = action

    def to_string(self):
        return '<node %s: [match %s -> action %s]>' %(self.node,self.match,self.action)


class UpdateOperation(object):
    def __init__(self, kind):
        self.kind = kind

    def __repr__(self):
        return self.to_string()
 
    def to_string(self):
        return ''


class OneRuleUpdateOperation(UpdateOperation):
    def __init__(self, kind, rule):
        UpdateOperation.__init__(self, kind)
        self.rule = rule

    def set_rule(self, rule):
        self.rule = rule

    def to_string(self):
        return '%s %s' %(self.kind, self.rule.to_string())


class AddOperation(OneRuleUpdateOperation):
    def __init__(self,rule):
        OneRuleUpdateOperation.__init__(self,'ADD',rule)


class DelOperation(OneRuleUpdateOperation):
    def __init__(self, rule):
        OneRuleUpdateOperation.__init__(self, 'DEL', rule)


class TwoRulesUpdateOperation(UpdateOperation):
    def __init__(self, kind, rule_pair):
        UpdateOperation.__init__(self, kind)
        self.rules = rule_pair

    def set_rules(self,rule_pair):
        self.rules = rule_pair

    def to_string(self):
        return '%s %s to %s' %(self.kind, self.rules[0].to_string(), self.rules[1].to_string())


class ChangeOperation(TwoRulesUpdateOperation):
    def __init__(self, rule_pair):
        TwoRulesUpdateOperation.__init__(self, 'CHG', rule_pair)


##################################
# Merged DAG preparing functions #
##################################

def compress_graph(graph):
    # initialization
    compressed = graph.copy()
    # remove nodes that cannot generate constraints
    for node in graph.nodes():
        # skip red nodes
        if graph.node[node]['color'] == 'red':
            continue
        # skip sinks
        succs = graph.successors(node)
        if len(succs) == 0:
            continue
        # if all its outgoing edges are in both the initial and final DAG, remove the current node and redistribute its incoming edges to its successors 
        to_remove = True
        for s in succs:
            if graph[node][s]['label'] != 'both':
                to_remove = False
        if to_remove:
            preds = compressed.predecessors(node)
            for p in preds:
                for s in succs:
                    compressed.add_edge(p,s,label=graph.edge[p][node]['label'])
            compressed.remove_node(node)
    return compressed


#################
# Main function #
#################
# TODO: how to treat multiple destinations? Can I say that this is a scheduling or bin packing problem (without weights but with constraints between objects)? Can I collect posets on a per-destination basis and say that constraints for different destinations can be ordered/scheduled independently (for example, returning a dictionary of poset per destination)?
def compute_sequence(graph, duplicate=False):
    """Finds an ordering that is loop-free and satisfies policy constraints.

    Args:
        graph (Graph) the Graph object collecting initial and final forwarding
              as well as the destination, the sources, and the policy paths.

    Returns:
        a MigrationStrategy object that encapsulates the sequence of operations
        that reconfigure the network from the initial to the final state while
        satisfying all constraints, or None if it doesn't exist.
    """
    storage = ConstraintStorage(
        graph, optimization_strategy=NaiveMinimizationStrategy(),
        duplicate=duplicate)
    storage.extract()
    log.debug('Initial constraints:\n%s', storage)
    new_storage = storage.select_best_alternatives()
    log.debug('Constraints after manipulation:\n%s', new_storage)
    migration = verify.MigrationFactory.create_migration_strategy(
        graph, new_storage,
        lplib.Solver(
            new_storage.get_active_constraints(),
            new_storage.get_variables()))
    return migration
    # NOTE: a more general step in the case of multiple destinations consists in translating the constraints between nodes into constraints between operations. However, considering the operations for all the destinations altogether can generate a large problem to solve.
