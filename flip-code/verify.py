import collections
import copy
import logging


logging.config.fileConfig("logging.conf")
log = logging.getLogger(__name__)


class Error(Exception):
    """A simple error class for this package."""


class CycleError(Error):
    """A cycle has been detected in the network."""


class BlackholeError(Error):
    """A blackhole has been detected in the network."""


class PolicyViolationError(Error):
    """A policy has been violated."""


class InvalidLabelError(Error):
    """A label other than 'initial' or 'final' was specified."""


class InvalidNetworkConfigurationError(Error):
    """Either the initial or the final state are incorrect."""


class UninitializedGraphError(Error):
    """We were given a graph that is not yet initialized."""


def is_subsequence(small, big):
    """Returns True if small is a subsequence of big."""
    return any(
        big[pos:(pos + len(small))] == small
        for pos in range(0, len(big) - len(small) + 1))


_INITIAL = 'initial'
_FINAL = 'final'
VALID_LABELS = set([_INITIAL, _FINAL])


class MigrationStrategy(object):
    """A migration strategy is a partially ordered set of operations.

    Attributes:
        poset (dict<int,list<Operation>>) maps a step number to a list of
              operations that can be performed in any order
        next_step (int) the next valid key in the poset
        network (Network)
    """
    INITIAL_STEP = 0
    FINAL_STEP = -1

    def __init__(self, network):
        """Initializes a migration on the given digraph.

        Args:
            net (Network) the underlying network
        """
        self._pre_update = []
        self._post_update = []
        self._poset = collections.defaultdict(list)
        self._next_step = 1
        self.network = network

    def __str__(self):
        steps = ['\n*PREUPDATE*\n\t' + '\n\t'.join(
            [str(x) for x in self._pre_update])]
        for s in sorted(self._poset.keys()):
            steps.extend(['*STEP %s*\n\t' % s + '\n\t'.join(
            [str(x) for x in self._poset[s]])])
        steps.extend(['*POSTUPDATE*\n\t' + '\n\t'.join(
            [str(x) for x in self._post_update])])
        return "\n".join(steps)

    def get_operations_at_step(self, step):
        if step == self.INITIAL_STEP:
            return self._pre_update
        elif step == self.FINAL_STEP:
            return self._post_update
        elif step is not None:
            return self._poset[step]
        else:
            raise AttributeError('step cannot be None')

    def migrate_node(self, nodeid, step=None):
        if step is None:
            step = self._next_step
        op = Operation(nodeid, migrated=True)
        self.get_operations_at_step(step).append(op)

    def next(self):
        self._next_step += 1

    def tag_and_match_start(self, tag_nodes, tag):
        self._tag_and_match(tag_nodes, tag, True)

    def tag_and_match_end(self, tag_nodes, tag):
        self._tag_and_match(tag_nodes, tag, False)

    def _tag_and_match(self, tag_nodes, tag, is_start):
        """Creates tag and match operations.

        Args:
            tag_nodes  (list<NodeID>) the list of nodes that apply the tag
            tag        (Tag) the tag to be applied and matched
            is_start   (bool) True if we want to start tagging, False if we
                       want to stop doing it
        """
        step = None
        op = None
        for n in tag_nodes:
            if is_start:
                op = Operation(nodeid=n, add_tag=tag)
                step = self.INITIAL_STEP
            else:
                op = Operation(nodeid=n, del_tag=tag)
                step = self.FINAL_STEP
            self.get_operations_at_step(step).append(op)
        a = Operation.ADD_MATCH if is_start else Operation.DEL_MATCH
        op = Operation(nodeid=tag.node, match=tag, action=a)
        self.get_operations_at_step(step).append(op)

    def verify(self):
        """Verifies the migration strategy on the current network.

        Computes a random order that is consistent with the partial order
        dictated by the constraints, and verifies that after each step of
        the reconfiguration there are no loops, no blackholes, and no policy
        violations.

        Returns:
            True if the strategy is verified. Raises exception otherwise.

        Raises:
            one of the exceptions in Packet.visit() if the migration strategy
            doesn't satisfy the requirements.
        """
        total_duplicated_entries = 0
        for macro_step, steps in (
                [('pre-update', self._pre_update)] +
                [(i, self._poset[i]) for i in sorted(self._poset.keys())] +
                [('post-update', self._post_update)]):
            log.info('migration step %s, %d operations', macro_step, len(steps))
            for op in steps:
                log.debug('%s', op)
                n = self.network.get_node(op.nodeid)
                n.apply(op)
                if op.match and op.action == Operation.ADD_MATCH:
                    total_duplicated_entries += 1
                if not self.verify_current_configuration(self.network):
                    return False
        assert(total_duplicated_entries % 2 == 0)
        total_entries = len(self.network.nodes())
        log.info('migration completed using %d/%d duplicated entries',
                 total_duplicated_entries/2, total_entries)
        return True

    @staticmethod
    def verify_current_configuration(network, label=''):
        """Verifies a single network state."""
        try:
            for s in network.graph.srcs:
                p = Packet(src=s, dst=network.graph.dst)
                for path in p.visit(network, network.graph.subpaths):
                    log.log(5, path)
        except Error as e:
            raise InvalidNetworkConfigurationError(
                'in %s configuration: %s %s' %(label, type(e), e))
        return True


class MigrationFactory(object):
    @staticmethod
    def create_migration_strategy(graph, storage, lp_solver):
        """Creates MigrationStrategy objects from a set of LP constraints.

        Args:
            graph      (flip.Graph)
            storage    (flip.ConstraintStorage) the (feasible) set of
                       LP constraints
            lp_solver  (lplib.Solver) an LP solver

        Returns:
            a MigrationStrategy object
        """
        net = Network(graph)
        net.final()
        log.debug('verifying the input network, final state')
        MigrationStrategy.verify_current_configuration(net, label='final')
        net.initial()
        log.debug('verifying the input network, initial state')
        MigrationStrategy.verify_current_configuration(net, label='initial')
        strategy = MigrationStrategy(net)
        for nodes in lp_solver.get_solution():
            for nodeid in nodes:
                strategy.migrate_node(nodeid)
            strategy.next()
        for c in storage.get_matching_constraints():
            taggers = c.tag_initial | c.tag_final
            for fib in (_INITIAL, _FINAL):
                tag = Tag(fib, c.node, graph.dst)
                strategy.tag_and_match_start(taggers, tag)
                strategy.tag_and_match_end(taggers, tag)
        return strategy


class Tag(object):
    """A tag that can be applied to a packet.

    Attributes:
        fib     (str) either _INITIAL or _FINAL, the fib entry this tag is
                expected to trigger
        node    (NodeID) the ID of the node that will match this tag
        dst     (NodeID) the ID of the destination for which this tag applies
        key     (tuple<NodeID, NodeID) a unique key that identifies this tag
                in a Packet. A Packet cannot carry multiple tags with the same
                key.
    """
    def __init__(self, fib, node, dst):
        if fib not in VALID_LABELS:
            raise InvalidLabelError(fib)
        self.node = node
        self.dst = dst
        self.fib = fib
        self.key = (self.node, self.dst)

    # __eq__ and __hash__ need be redefined to allow deep copies of a Tag
    # object to match in hashable collections of Tags.
    def __eq__(self, other):
        return (self.node == other.node and
                self.dst == other.dst and
                self.fib == other.fib)

    def __hash__(self):
        return (hash(self.node) ^ hash(self.dst) ^ hash(self.fib))

    def __repr__(self):
        return '(node=%s, dst=%s) => %s' %(self.node, self.dst, self.fib)


class Operation(object):
    """A migration operation on a single node.

    An operation can be applied to a single node, and it changes its
    forwarding behavior by instructing it to add/remove tags and to use
    either the final or the initial FIB entry.

    Attributes:
        nodeid   (NodeID) the ID of the node this operation refers to
        add_tag  (str) the tag to add, if any
        del_tag  (str) the tag to remove, if any
        match    (Tag) the Tag object we want to add or remove
        migrated (bool) if False (True) the node shall forward using the initial
                        (final) FIB entry
    """
    ADD_MATCH = 'add'
    DEL_MATCH = 'del'

    def __init__(self, nodeid, add_tag=None, del_tag=None,
                 match=None, migrated=False, action=None):
        self.nodeid = nodeid
        self.add_tag = add_tag
        self.del_tag = del_tag
        self.migrated = migrated
        self.match = match
        if match:
            assert(action in [self.ADD_MATCH, self.DEL_MATCH])
            self.action = action

    def __str__(self):
        strings = ['OP<%s>:' % self.nodeid]
        if self.migrated:
            strings.append('migrate')
        if self.add_tag:
            strings.append('add-tag %s' % self.add_tag)
        if self.del_tag:
            strings.append('del-tag %s' % self.del_tag)
        if self.match:
            strings.append('%s-match %s' % (self.action, str(self.match)))
        return " ".join(strings)


class Node(object):
    """A node models a router in the network.

    Attributes:
        nodeid       (NodeID) the ID of the node in the network
        next_initial (list<NodeID>) the nexthops according to the initial FIB
        next_current (list<NodeID>) the nexthops according to the current FIB
        next_final   (list<NodeID>) the nexthops according to the final FIB
        tags         (list<Tag>) the list of tags to push onto the packet
        _next_by_tag (dict<tag, list<NodeID>) the nexthops when tag is found.
                     Tags are exclusive (i.e., we cannot match more than
                     one tag in a packet. The nexthops are either next_initial
                     or next_final.
    """
    def __init__(self, nodeid, next_initial, next_final, tags=None):
        self._next_by_tag = {}
        self.nodeid = nodeid
        self.next_initial = next_initial
        self.next_current = next_initial
        self.next_final = next_final
        self.tags = tags or []

    def _reset_to(self, nexthops):
        self._next_by_tag.clear()
        self.next_current = nexthops

    def apply(self, operation):
        """Applies an Operation to this node.

        Arguments:
            operation (Operation): the operation object to apply
        """
        if operation.migrated:
            self.next_current = self.next_final
        if operation.add_tag:
            self.tags.append(operation.add_tag)
        if operation.del_tag:
            self.tags.remove(operation.del_tag)
        if operation.match:
            if operation.match.fib == _INITIAL:
                self._next_by_tag[operation.match] = self.next_initial
            elif operation.match.fib == _FINAL:
                self._next_by_tag[operation.match] = self.next_final
            else:
                raise InvalidLabelError(operation.match.fib)

    def forward(self, packet):
        """Forwards a packet according to the current state.

        Possibly modifies the packet by adding/removing tags.

        Args:
            packet (Packet) the packet to be forwarded

        Returns:
            the nexthop (NodeID)
        """
        nexthop = self.next_current
        for tag in packet.get_tags():
            if tag in self._next_by_tag:
                nexthop = self._next_by_tag[tag]
                break
        for tag in self.tags:
            if (
                    (tag.fib == _INITIAL and
                     nexthop is self.next_initial) or
                    (tag.fib == _FINAL and
                     nexthop is self.next_final)):
                packet.add_tag(tag)
        return nexthop

    def to_final(self):
        self._reset_to(self.next_final)

    def to_initial(self):
        self._reset_to(self.next_initial)


class Packet(object):
    """A packet is basically a set of tags and a source/destination pair.

    Attributes:
        src  (NodeID) the node that originated this packet
        dst  (NodeID) the node this packet is destined to
        path (list<NodeID>) the path this packet as done so far
        tags (dict<tag_key, Tag>) the tags that this packet carries
    """
    def __init__(self, src, dst):
        self.src = src
        self.dst = dst
        self.path = []
        self.tags = {}

    def __str__(self):
        return "Packet<src: %s, dst: %s, path: %s, tags: %s>" %(
            self.src, self.dst, self.path, self.tags)

    def add_tag(self, tag):
        self.tags[tag.key] = tag

    def get_tags(self):
        return self.tags.values()

    def visit(self, network, policy_paths):
        """Visits the network in the current state.

        Enumerate all possible paths using a DFS.
        Either we reach the destination, we find a blackhole, or a loop.
        For nodes with multiple nexthops, we duplicate the packet as each
        path could add/remove specific tags.

        Args:
            network      (Network) the network we want to visit
            policy_paths (list<list<NodeID>>) a list of paths that the packet
                         should traverse (ANY traversed path will satisfy the
                         requirement)

        Returns:
            a generator of all paths from a source to the destination,

        Raises:
            BlackholeError, if one of the visited paths is a blackhole
            CycleError, if one of the visited paths is a cycle
            PolicyViolationError, if one of the visited paths doesn't contain
                                  any of policy_paths

        NOTE: only one exception is raised. If a network presents multiple
        problematic paths, one exception will be raised (at random).
        """
        stack = collections.deque()
        state = {'packet': self, 'node': network.get_node(self.src).nodeid}
        stack.append(state)
        while len(stack) > 0:
            state = stack.pop()
            state['packet'].path.append(state['node'])
            if state['packet'].path.count(state['node']) > 2:
                raise CycleError("%s %s" %(state['packet'], nexthop))
            nexthops = network.get_node(state['node']).forward(state['packet'])
            if len(nexthops) < 1:
                # We reached a sink node.
                if state['node'] != state['packet'].dst:
                    raise BlackholeError(state['packet'].path)
                if policy_paths and not any(
                        [is_subsequence(subpath, state['packet'].path)
                        for subpath in policy_paths]):
                    raise PolicyViolationError(
                        '%s doesn\'t satisfy any of %s' %(
                            state['packet'], policy_paths))
                yield state['packet'].path
            for i, nexthop in enumerate(nexthops):
                if i == len(nexthops) - 1:
                    # Don't copy the packet for the last nexthop in the list.
                    state['node'] = nexthop
                    stack.append(state)
                else:
                    other_state = {'packet': copy.deepcopy(state['packet']),
                                   'node': nexthop}
                    stack.append(other_state)


class Network(object):
    """A network is a graph made of Nodes and their interconnections.

    Attributes:
        graph (flip.Graph) the underlying initial and final graph
        _nodes  (dict<NodeID, Node>) a map from an ID to the Node object having
                that ID. The NodeID in this dict matches the one in graph
    """
    def __init__(self, graph):
        if (
                not graph.initial.has_node(graph.dst) or
                'color' not in graph.initial.node[graph.dst]):
            raise UninitializedGraphError
        self.graph = graph
        self._nodes = {}
        for nodeid in self.nodes():
            next_initial = []
            next_final = []
            if graph.initial.has_node(nodeid):
                next_initial = graph.initial.successors(nodeid)
            if graph.final.has_node(nodeid):
                next_final = graph.final.successors(nodeid)
            self._nodes[nodeid] = Node(nodeid, next_initial, next_final)

    def nodes(self):
        return set(self.graph.initial.nodes()) | set(self.graph.final.nodes())

    def get_node(self, nodeid):
        return self._nodes[nodeid]

    def final(self):
        """Resets the network configuration to the final state."""
        for nodeid in self._nodes:
            self._nodes[nodeid].to_final()

    def initial(self):
        """Resets the network configuration to the initial state."""
        for nodeid in self._nodes:
            self._nodes[nodeid].to_initial()

