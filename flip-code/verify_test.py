import copy
import flip
import networkx as nx
import unittest
import verify


class Gadgets():
    _instance = None

    def __new__(cls, *args, **kwargs):
        if (cls._instance is None):
            cls._instance = super.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        g = nx.path_graph(30, create_using=nx.DiGraph())
        g.add_edge(28, 12)
        self.cycle_graph = flip.Graph(g, g, 29)
        self.cycle_graph.initialize()
        g = nx.complete_graph(10, create_using=nx.DiGraph())
        self.complete_graph = flip.Graph(g, g, 9)
        self.complete_graph.initialize()
        g = nx.path_graph(30, create_using=nx.DiGraph())
        self.path_graph = flip.Graph(g, g, 29, srcs=[0])
        self.path_graph.initialize()
        g = nx.path_graph(30, create_using=nx.DiGraph())
        g.add_edge(5, 10)
        self.multipath_graph = flip.Graph(g, g, 29)
        self.multipath_graph.initialize()
        self.multipath_graph_shortcut_missing = [6, 7, 8, 9]
        g = g.copy()
        g.add_edge(10, 100)
        self.multipath_graph_blackhole = flip.Graph(g, g, 29)
        self.multipath_graph_blackhole.initialize()

    
class NetworkTest(unittest.TestCase):
    def setUp(self):
        self.gadgets = Gadgets()

    def testSimplePathNetwork(self):
        g = self.gadgets.path_graph
        net = verify.Network(g)
        for i in range(len(net.nodes()) - 1):
            self.assertNotEquals(net.get_node(i), None)

    def testUninitializedGraph(self):
        with self.assertRaises(verify.UninitializedGraphError):
            verify.Network(flip.Graph(nx.DiGraph(), nx.DiGraph(), 0))

        
class NodeTest(unittest.TestCase):
    def setUp(self):
        self.initial = [2, 3]
        self.final = [4, 5]
        self.node = verify.Node(1, self.initial, self.final)
        self.operation = verify.Operation(nodeid=self.node.nodeid,
                                          migrated=True)
        self.packet = verify.Packet(self.node.nodeid, 2)
        self.tag_initial = verify.Tag(
            verify._INITIAL, self.node.nodeid, self.packet.dst)
        self.tag_final = verify.Tag(
            verify._FINAL, self.node.nodeid, self.packet.dst)

    def testForwardSimple(self):
        self.assertEquals(self.node.forward(self.packet), self.initial)
        self.node.apply(self.operation)
        self.assertEquals(self.node.forward(self.packet), self.final)

    def testForwardAddTags(self):
        self.node.apply(
            verify.Operation(nodeid=self.node.nodeid, add_tag=self.tag_initial))
        self.node.forward(self.packet)
        self.assertIn(self.tag_initial, self.packet.get_tags())

    def testForwardAddTagsAccordingToState(self):
        self.node.apply(
            verify.Operation(nodeid=self.node.nodeid, add_tag=self.tag_initial))
        self.node.apply(
            verify.Operation(nodeid=self.node.nodeid, add_tag=self.tag_final))
        self.node.forward(self.packet)
        self.assertIn(self.tag_initial, self.packet.get_tags())
        self.assertNotIn(self.tag_final, self.packet.get_tags())
        self.node.apply(
            verify.Operation(nodeid=self.node.nodeid, migrated=True))
        self.node.forward(self.packet)
        self.assertIn(self.tag_final, self.packet.get_tags())
        self.assertNotIn(self.tag_initial, self.packet.get_tags())

    def testForwardAddsConsistentTags(self):
        """If we matched a tag, we add the corresponding tag."""
        tag1 = verify.Tag(verify._INITIAL, self.node.nodeid, self.packet.dst)
        tag2 = verify.Tag(verify._INITIAL, 'someothernode', self.packet.dst)
        tag3 = verify.Tag(verify._FINAL, self.node.nodeid, self.packet.dst)
        tag4 = verify.Tag(verify._FINAL, 'someothernode', self.packet.dst)
        packet_with_tag1 = verify.Packet('somesource', self.packet.dst)
        packet_with_tag1.add_tag(tag1)
        packet_with_tag3 = verify.Packet('somesource', self.packet.dst)
        packet_with_tag3.add_tag(tag3)
        self.node.apply(verify.Operation(
            nodeid=self.node.nodeid, match=tag1,
            action=verify.Operation.ADD_MATCH))
        self.node.apply(verify.Operation(nodeid=self.node.nodeid, add_tag=tag2))
        self.node.apply(verify.Operation(
            nodeid=self.node.nodeid, match=tag3,
            action=verify.Operation.ADD_MATCH))
        self.node.apply(verify.Operation(nodeid=self.node.nodeid, add_tag=tag4))
        for migrated in (False, True):
            self.node.apply(verify.Operation(nodeid=self.node.nodeid,
                                             migrated=migrated))
            self.node.forward(packet_with_tag1)
            self.assertIn(tag2, packet_with_tag1.get_tags())
            self.assertNotIn(tag4, packet_with_tag1.get_tags())
            self.node.forward(packet_with_tag3)
            self.assertIn(tag4, packet_with_tag3.get_tags())
            self.assertNotIn(tag2, packet_with_tag3.get_tags())

    def testForwardSwapsTagsIfKeyPresent(self):
        tag1 = verify.Tag(verify._INITIAL, self.node.nodeid, self.packet.dst)
        tag2 = verify.Tag(verify._FINAL, self.node.nodeid, self.packet.dst)
        self.node.apply(
            verify.Operation(nodeid=self.node.nodeid, add_tag=tag2))
        self.node.apply(
            verify.Operation(nodeid=self.node.nodeid, migrated=True))
        self.packet.add_tag(tag1)
        self.node.forward(self.packet)
        self.assertIn(tag2, self.packet.get_tags())
        self.assertNotIn(tag1, self.packet.get_tags())

    def testForwardBasedOnTags(self):
        self.node.apply(self.operation)
        self.assertEquals(self.node.forward(self.packet), self.final)
        self.node.apply(
            verify.Operation(nodeid=self.node.nodeid,
                             match=self.tag_initial,
                             action=verify.Operation.ADD_MATCH))
        self.packet.add_tag(self.tag_initial)
        self.assertEquals(self.node.forward(self.packet), self.initial)

    def testForwardBasedOnCopyOfTags(self):
        self.node.apply(self.operation)
        self.assertEquals(self.node.forward(self.packet), self.final)
        self.node.apply(
            verify.Operation(nodeid=self.node.nodeid,
                             match=copy.deepcopy(self.tag_initial),
                             action=verify.Operation.ADD_MATCH))
        self.packet.add_tag(copy.deepcopy(self.tag_initial))
        self.assertEquals(self.node.forward(self.packet), self.initial)

class PacketTest(unittest.TestCase):
    def _get_packet(self):
        return verify.Packet(0, self.net.graph.dst)

    def setUp(self):
        self.gadgets = Gadgets()
        self.net = verify.Network(self.gadgets.path_graph)
        self.policy_path = [8]

    def testVisitPathNetworkNoPolicy(self):
        self.assertTrue(self._get_packet().visit(self.net, [self.policy_path]))

    def testVisitPathNetworkMixedPolicy(self):
        policy_paths = [[8, 5], [8, 6], [10]]
        self.assertTrue(self._get_packet().visit(self.net, policy_paths))

    def testVisitPathNetworkInfeasiblePolicy(self):
        self.policy_path = [8, 5]
        with self.assertRaises(verify.PolicyViolationError):
            list(self._get_packet().visit(self.net, [self.policy_path]))

    def testVisitPathNetworkBlackhole(self):
        packet = verify.Packet(0, len(self.net.nodes()))
        with self.assertRaises(verify.BlackholeError):
            list(packet.visit(self.net, [self.policy_path]))

    def testVisitCycleNetworkLoop(self):
        self.net = verify.Network(self.gadgets.cycle_graph)
        with self.assertRaises(verify.CycleError):
            list(self._get_packet().visit(self.net, [self.policy_path]))

    def testVisitMultipathNetworkAllSatisfyPolicy(self):
        self.net = verify.Network(self.gadgets.multipath_graph)
        after_shortcut = self.gadgets.multipath_graph_shortcut_missing[-1] + 1
        self.assertTrue(self._get_packet().visit(self.net, [[after_shortcut]]))

    def testVisitMultipathNetworkSomeDontSatisfyPolicy(self):
        self.net = verify.Network(self.gadgets.multipath_graph)
        for skipped_node in self.gadgets.multipath_graph_shortcut_missing:
            with self.assertRaises(verify.PolicyViolationError):
                list(self._get_packet().visit(self.net, [[skipped_node]]))

    def testVisitMultipathNetworkSomeBlackholes(self):
        self.net = verify.Network(self.gadgets.multipath_graph_blackhole)
        with self.assertRaises(verify.BlackholeError):
            list(self._get_packet().visit(self.net, []))


class MigrationStrategyTestCase(unittest.TestCase):
    def setUp(self):
        self.gadgets = Gadgets()

    def testVerifyCurrentConfigurationSucceeds(self):
        g = self.gadgets.path_graph
        self.assertTrue(
            verify.MigrationStrategy.verify_current_configuration(
                verify.Network(g)))

    def testVerifyCurrentConfigurationFailsPolicy(self):
        g = self.gadgets.path_graph.copy()
        g.subpaths = [[-1]]
        with self.assertRaises(verify.InvalidNetworkConfigurationError):
            verify.MigrationStrategy.verify_current_configuration(
                verify.Network(g))

    def testVerifyCurrentConfigurationFailsBlackhole(self):
        dag = nx.path_graph(30, create_using=nx.DiGraph())
        dag.add_edge(5, 101)
        g = flip.Graph(dag, dag, 29, srcs=[1])
        g.initialize()
        with self.assertRaises(verify.InvalidNetworkConfigurationError):
            verify.MigrationStrategy.verify_current_configuration(
                verify.Network(g))


if __name__ == '__main__':
    unittest.main()
