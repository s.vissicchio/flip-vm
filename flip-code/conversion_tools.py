#!/usr/bin/python
# Convert an ENTF file into a networkx graph
import logging
import logging.config
import math
import networkx as nx
import random
import re
import sys


logging.config.fileConfig("logging.conf")
log = logging.getLogger(__name__)


def get_networkx_from_ntf(input_file, existing_graph=None):
    """Get a networkx Graph from a NTF representation of a network.

    Args:
      input_file     (str) the pathname to the .ntf file.
      existing_graph (nx.Graph or nx.DiGraph) an existing graph we want to
                     add edges to. This makes it possible to get either a
                     Graph or a DiGraph out of the same file.

    Returns:
      existing_graph if it was not None, otherwise a networkx.Graph object.
    """
    # Some characters will create problems when we convert a node label to a
    # variable in an ILP problem, so we convert them straight away.
    # Make sure '-' is the last character otherwise it will be interpreted as
    # a range.
    BAD_CHARS = r'[*^+-]'
    graph = nx.Graph()
    if existing_graph is not None:
        graph = existing_graph
    with open(input_file) as ntf:
        for line in ntf:
            splitted = line.split()
            node1 = re.sub(BAD_CHARS, '_', splitted[0])
            node2 = re.sub(BAD_CHARS, '_', splitted[1])
            graph.add_edge(node1, node2,
                           weight=int(math.floor(float(splitted[2]))))
    return graph


def output_networkx_to_ntf(graph, output_file):
    """Saves a networkx.Graph object in NTF format.

    Args:
        graph, networkx.Graph, the graph we want to save
        output_file, str, the pathname of the file we want to write
    """
    with open(output_file, 'w') as out:
        for (a, b, attributes) in graph.edges(data=True):
            out.write("{} {} {}\n".format(a, b, attributes.get('weight', 1)))


def get_path_requirements_from_file(input_file):
    '''
        This function returns a list of path requirements
    '''
    
    path_requirements = []    
    input_ = open(input_file)
    for line in input_:
        path_requirements.append(eval(line))
    input_.close()
    return path_requirements


def get_node_list_from_file(input_file):
    '''
        This function returns a list of nodes
    '''
    
    if input_file is None:
	input_file = ""
    nodes = []
    try:
    	input_ = open(input_file)
    	for line in input_:
            nodes.append(str(line.strip()))
    	input_.close()
    except IOError:
	pass
    return nodes

# returns the entire graph described in the entf file
def get_networkx_from_entf(input_file):
    '''
        This function returns a networkx DiGraph object based on an entf representation of a network
    '''
    entf_input = open(input_file)
    graph = nx.DiGraph()
    zone_names = set([])

    for line in entf_input:
        splitted = line.split()

	#if len(splitted) < 5:
	if len(splitted) < 4:
		print "error in file format in line " + str(line)
		continue

        #graph.add_edge(splitted[0], splitted[1], weight=int(splitted[2]), role=str(splitted[3]), zone=str(splitted[4]))

	n1 = splitted[0] 
	n2 = splitted[1]
	w = splitted[2]
	z = splitted[3]
	graph.add_edge(n1, n2, weight=int(w), zone=str(z))
	zone_names.add(z)
	
	for node in [n1,n2]:
		props = graph.node[str(node)]
		if "zone" not in props.keys():
			props["zone"] = z
		elif z not in props["zone"].split():
				props["zone"] += " " + z

    entf_input.close()

    #print graph

    return (graph,zone_names)

# computes the subgraph containing only the links that are in the zone given as input
def compute_subgraph(graph,curr_zone):
        sub = nx.Graph()
        for (n1,n2) in graph.edges():
                if graph[n1][n2]["zone"] == curr_zone:
                        w = graph[n1][n2]["weight"]
                        sub.add_edge(n1,n2,weight=w,zone=curr_zone)
        return sub

# returns a dictionary, in which keys are names of the zones and values are networkx graphs for the different zones in the network
def get_zones_from_entf(input_file):
	zones_dict = dict()
	(graph,zone_names) = get_networkx_from_entf(input_file)
	for name in zone_names:
		subgraph = compute_subgraph(graph,name)
		zones_dict[name] = subgraph
	return zones_dict


def get_perturbated_graph(graph, nlinks=5, weights=None):
    """Returns a copy of the input graph with randomly changed link weights.

    Args:
      graph     (nx.Graph or nx.DiGraph) the input graph. Won't be modified.
      nlinks    (int) the number of links we want to change the weight of.
      weights   (list<int>) a collection of weights to randomly choose from.
                If None the actual weights in graph are used.

    Returns:
      a copy of graph where the weight of nlinks links has been randomly picked
      from the given collection of weights.
    """
    perturbated = graph.copy()
    all_links = perturbated.edges(data=True)
    if weights is None:
        weights = [link[2]['weight'] for link in all_links]
    for link in random.sample(all_links, nlinks):
        prev_weight = link[2]['weight']
        new_weight = prev_weight
        while new_weight == prev_weight:
            new_weight = random.choice(weights)
        link[2]['weight'] = new_weight
        log.debug("Link %s weight changed: %d => %d", link[:2], prev_weight, new_weight)
    return perturbated
