# /usr/bin/python
# -*- coding: utf-8 -*-

from pulp import *
import abc
import collections
import gurobipy
import os
import sys
import tempfile


class ConvertibleToILPConstraint(object):
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def to_list(self):
        """Returns a list of pairs of lists.

        The semantics is that for each pair we impose that at least one
        element in the left hand side is < then at least one element in
        the right hand side.

        For instance,
        [(['a', 'b'], ['c', 'd']), (['e'], ['f'])]
        implies that e<f, and any of the following
           a<c, a<d, b<c, b<d.
        """


class Solver:
    """Implements an ILP solver.

    Attributes:
      constraint_list: a list of ConvertibleToILPConstraint objects.
      node_names: list of strings. The variables in the ILP problem.
      strict_sequence: bool. Whether we require a strict ordering.
      verbose: int. Verbosity level.
    """
    def __init__(self, constraint_list, node_names,
                 strict_sequence=False, verbose=0):
        self.node_names = node_names
        self.constraint_list = constraint_list
        self.strict_sequence = strict_sequence
        self.lp2con_mapping = {} 
        self.computedLp = {}
        self.gurobi_model = None
        self.solution = None
        self.verbose = verbose
        if verbose < 1:
            gurobipy.setParam("OutputFlag", 0)

    def translate_to_lp(self):
        if self.computedLp.has_key(hash(str(self.constraint_list))):
            return
        if not self.constraint_list:
            self.gurobi_model = None
            return
        self.lp2con_mapping = {}
        node_var = LpVariable.dicts("Time", self.node_names, 0, sys.maxint, LpInteger)
        prob = LpProblem("Router Migration Ordering Problem", LpMinimize)
        prob += lpSum(node_var), "Minimize values of variables"
        index_bin_var = 0
        index_c = 0
        for con in self.constraint_list:
            if not isinstance(con, ConvertibleToILPConstraint):
                continue
            for first_mem, second_mem in con.to_list():
                bin_var_list = []
                for u in first_mem:
                    for v in second_mem:
                        bin_var = LpVariable("Bin" + str(index_bin_var),cat=LpBinary)
                        bin_var_list.append(bin_var)
                        ilp_constraint = (node_var[u] - 10000 * bin_var <=
                                          node_var[v] - 1)
                        self.add_LP_constraint(
                            prob, ilp_constraint, index_c, con)
                        index_c += 1
                        index_bin_var += 1
                self.add_LP_constraint(
                    prob, lpSum(bin_var_list) <= len(bin_var_list) - 1,
                    index_c, con)
                index_c += 1
        if self.strict_sequence:
            index_sel_var = 0
            visited=set()
            for f in node_var:
                visited.add(f)
                for s in node_var:
                    if s != f:
                        sel_var_list = []
                        sel_var1 = LpVariable("Sel" + str(index_sel_var),cat=LpBinary)
                        sel_var_list.append(sel_var1)
                        index_sel_var += 1
                        sel_var2 = LpVariable("Sel" + str(index_sel_var),cat=LpBinary)
                        sel_var_list.append(sel_var2)
                        index_sel_var += 1
                        prob += node_var[f] - 10000 * sel_var1 <= node_var[s] - 1
                        prob += node_var[s] - 10000 * sel_var2 <= node_var[f] - 1
                        prob += lpSum(sel_var_list) <= len(sel_var_list) - 1
        (fd, filename) = tempfile.mkstemp(suffix='.lp')
        prob.writeLP(filename)
        self.gurobi_model = gurobipy.read(filename)
        os.close(fd)
        if self.verbose:
            print "LP problem written to", filename
        else:
            os.remove(filename)
        self.computedLp[hash(str(self.constraint_list))] = 1

    def add_LP_constraint(self, prob, ilp_con, index, constraint):
        ilp_con.name = "OC" + str(index)
        self.lp2con_mapping[ilp_con.name] = constraint
        prob += ilp_con

    def get_solution(self):
        if self.solution is None:
            self.solution = self._solve()
        return self.solution
        
    def _solve(self):
        if not self.constraint_list:
            return [self.node_names]
        self.translate_to_lp()
        self.gurobi_model.optimize()
        if self.gurobi_model.status != gurobipy.GRB.status.OPTIMAL:
            return []
        dict_time = collections.defaultdict(list)
        for v in self.gurobi_model.getVars():
            if v.VarName.startswith("Bin") or v.VarName.startswith("Sel"):
                continue
            val = int(v.X)
            dict_time[val].append(v.VarName.replace("Time_", ""))
        ordering = []
        for k in sorted(dict_time.keys()):
            if self.strict_sequence:
                ordering += dict_time[k]
            else:
                ordering.append(dict_time[k])
        return ordering

    def compute_IIS(self):
        self.translate_to_lp()
        potential_solution = self._solve()
        if potential_solution:
            self.solution = potential_solution
            return []
        self.gurobi_model.computeIIS()
        iis_cons = {}
        for c in self.gurobi_model.getConstrs():
            if c.getAttr(gurobipy.GRB.attr.IISConstr) == 1:
                original_c = self.lp2con_mapping[c.getAttr("ConstrName")]
                if original_c is not None:
                    iis_cons[str(original_c)] = original_c
        return iis_cons.values()
