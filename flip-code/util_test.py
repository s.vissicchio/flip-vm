import networkx as nx
import unittest
import util

class EnumeratePathWithCyclesTestCase(unittest.TestCase):
    def setUp(self):
        self.graph = nx.DiGraph()
        self.graph.add_path(range(10))
        self.graph.add_edge(3, 7)
        self.graph.add_edge(8, 1)  # creates a loop
        self.graph.add_edge(1, 9)

    def testEnumerationWithoutCycles(self):
        self.assertItemsEqual(
            [
                [1, 2, 3, 4, 5, 6, 7, 8, 9],
                [1, 9],
                [1, 2, 3, 7, 8, 9],
            ],
            util.all_paths_with_cycles(self.graph, 1, 9, 1)
        )

    def testEnumerationWithOneCycle(self):
        self.assertItemsEqual(
            [
                [1, 2, 3, 4, 5, 6, 7, 8, 9],
                [1, 2, 3, 4, 5, 6, 7, 8, 1, 9],
                [1, 9],
                [1, 2, 3, 7, 8, 9],
                [1, 2, 3, 7, 8, 1, 9],
            ],
            util.all_paths_with_cycles(self.graph, 1, 9, 2)
        )

    def testEnumerationWithNoPaths(self):
        for i in range(1, 4):
            self.assertItemsEqual(
                [], util.all_paths_with_cycles(self.graph, 9, 1, i)
            )
