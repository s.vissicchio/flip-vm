def all_paths_with_cycles(digraph, src, dst, max_occurrences=2):
    """Generate paths from src to dst, possibly including cycles.

    The generated paths are guaranteed to start at src, end at dst,
    and at most a single node can appear max_occurrences times in the
    path (all other nodes appear fewer times).

    We do a DFS and we keep a current path and current visit count for
    each node in the path so far. When a node has been seen max_occurrences
    times, we decrease the number of occurrences we allow for the visit.

    Args:
        digraph: a networkx.DiGraph() object
        src: a nodeID which must exist in digraph
        dst: a nodeID which must exist in digraph
        max_occurrences: int, the maximum number of occurrences for a single
                         node that we are willing to accept.

    Yields:
        a path as a list of nodeIDs
    """
    stack = [
        # node, path, dictionary of occurrences, max occurrences accepted
        (src, [src], {}, max_occurrences)
    ]
    while stack:
        node, path, count, accept_count = stack.pop()
        if node == dst:
            yield path
            continue
        count.setdefault(node, 0)
        count[node] += 1
        if count[node] == accept_count:
            accept_count = max(0, accept_count-2)
        for succ in digraph.successors(node):
            if count.get(succ, 0) <= accept_count:
                stack.append((succ, path+[succ], count.copy(), accept_count))


# Find frontier green-green and white-white nodes (with a closure).
def enumerate_frontier(dag, from_nodes, is_frontier):
    """Enumerate the frontier in a graph where a condition holds.
    
    Args:
        dag: a networkx.DiGraph object
        from_nodes: the nodeIDs where we start our visit
        is_frontier: a function nodeID-->bool that determines whether a node
                     belongs to the frontier.

    Returns:
        a set() of nodes that are reachable from from_nodes and for which
        is_frontier is True.
    """
    visited = set()
    frontier = set()
    for node in from_nodes:
        stack = [node]
        while stack:
            n = stack.pop()
            if n in visited:
                continue
            visited.add(n)
            if is_frontier(n):
                frontier.add(n)
                continue
            stack.extend(dag.successors(n))
    return frontier
