#!/usr/bin/python2

import abc
import argparse
import collections
import conversion_tools as convtools
import decimal
import errno
import flip
import logging
import logging.config
import math
import modified_dijkstra as md
import networkx as nx
import operator
import os
import random
import pdb
import signal
import sys
import time
import verify


log = logging.getLogger()


class PolicyGenerationError(Exception):
    pass


class PolicyRegistry(object):
    _registry = {}

    @classmethod
    def register(cls, name, generator):
        assert(isinstance(name, str))
        assert(issubclass(generator, AbstractPolicyGenerator))
        cls._registry[name] = generator

    @classmethod
    def all(cls):
        return cls._registry.keys()

    @classmethod
    def get(cls, name):
        return cls._registry[name]


class AbstractPolicyGenerator(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, graph, max_policies, max_mboxes):
        self.graph = graph
        self.max_policies = max_policies
        self.max_mboxes = max_mboxes

    @abc.abstractmethod
    def add_policies(self):
        """Adds policy subpaths to the flip.Graph object in self.graph."""


class EquivalenceClassPolicyGenerator(AbstractPolicyGenerator):
    def add_policies(self):
        triplets = set()
        for src in self.graph.srcs:
            for dag in (self.graph.initial, self.graph.final):
                for p in nx.all_simple_paths(dag, src, self.graph.dst):
                    if any([verify.is_subsequence(t, p)
                            for t in triplets]):
                        continue
                    i = random.randint(2, len(p) - 2)
                    triplet = (p[i-1], p[i], p[i+1])
                    log.debug('Adding policy %s for path %s', triplet, p)
                    triplets.add(triplet)
        self.graph.subpaths.extend([list(t) for t in triplets])
         
class InjectTripletsPolicyGenerator(AbstractPolicyGenerator):
    def add_policies(self):
        triplets = set()
        for src in self.graph.srcs:
            for dag in (self.graph.initial, self.graph.final):
                for p in nx.all_simple_paths(dag, src, self.graph.dst):
                    i = random.randint(2, len(p) - 2)
                    triplet = (p[i-1], p[i], p[i+1])
                    log.debug('Adding policy %s for path %s', triplet, p)
                    triplets.add(triplet)
        self.graph.subpaths.extend([list(t) for t in triplets])
                

class CentralityPolicyGenerator(AbstractPolicyGenerator):
    def add_policies(self):
        # First we compute centrality for triplets and middleboxes
        centrality = {
            'triplets': collections.defaultdict(lambda: 0),
            'mboxes': collections.defaultdict(lambda: 0),
        }
        for src in self.graph.srcs:
            for dag in (self.graph.initial, self.graph.final):
                for p in nx.all_simple_paths(dag, src, self.graph.dst):
                    for i in range(1, len(p) - 2):  # no dst in the triplet
                        node = p[i]
                        triplet = (p[i-1], p[i], p[i+1])
                        centrality['triplets'][triplet] += 1
                        centrality['mboxes'][node] += 1
        triplets = collections.OrderedDict(sorted(centrality['triplets'].items(),
                                           key=operator.itemgetter(1)))
        mboxes = collections.OrderedDict(sorted(centrality['mboxes'].items(),
                                                key=operator.itemgetter(1)))
        policies_ok = False
        added_triplets = 0
        added_mboxes = 0
        while added_triplets + added_mboxes == 0 or not policies_ok:
            if added_triplets < self.max_policies and triplets:
                triplet, betweenness = triplets.popitem()
                log.debug('Adding policy %s', triplet)
                self.graph.subpaths.append(list(triplet))
                added_triplets += 1
                for n in triplet:
                    if n in mboxes:
                        del mboxes[n]
            elif added_mboxes < self.max_mboxes and mboxes:
                mbox, betweenness = mboxes.popitem()
                log.debug('Adding mbox %s', mbox)
                self.graph.subpaths.append([mbox])
                added_mboxes += 1
            else:
                log.critical('no more policies to add (policies=%d, mboxes=%d)',
                             added_triplets, added_mboxes)
                raise PolicyGenerationError
            try:
                self.graph.initialize()
                n = verify.Network(self.graph)
                n.final()
                verify.MigrationStrategy.verify_current_configuration(n, 'final')
                n.initial()
                verify.MigrationStrategy.verify_current_configuration(n, 'initial')
                policies_ok = True
            except verify.InvalidNetworkConfigurationError:
                pass
        

def debug(sig, frame):
    pdb.set_trace()


def print_dag_stats(init_dag, fin_dag, graph):
    immutable = set()
    for dag, other_dag in [(init_dag, fin_dag),
                           (fin_dag, init_dag)]:
        immutable |= set(n for n in dag.nodes()
                           if (other_dag.has_node(n) and
                               dag.successors(n) == other_dag.successors(n)))
    return (
        '%d/%d nodes (%.2f%%) have the same nexthops' % (
            len(immutable), len(init_dag.nodes()),
            100.0 * len(immutable) / len(init_dag.nodes())
        )
    )


def main():
    # Register signal handler to drop into PDB
    signal.signal(signal.SIGUSR1, debug)
    # Register DEBUG$i logging levels
    for i in range(1, 10):
        logging.addLevelName(10 - i, 'DEBUG%d' % i)

    PolicyRegistry.register('centrality', CentralityPolicyGenerator)
    PolicyRegistry.register('inject', InjectTripletsPolicyGenerator)
    PolicyRegistry.register('equivalence', EquivalenceClassPolicyGenerator)

    parser = argparse.ArgumentParser(description='Run a FLIP migration.')
    parser.add_argument('--duplicate', dest='duplicate', type=bool, default=False,
                        help='Optimize for performance at the cost of extra '
                             'duplication of FIB entries')
    parser.add_argument('--topology', dest='topology', type=str, required=True,
                        help='The input NTF file (node <sp> node <sp> weight)')
    parser.add_argument('--dump_output_dir', dest='dump_output_dir', type=str,
                        help='A directory where we store the data to run the '
                        'GPIA algorithm on the same input instance, for easy '
                        'comparison')
    parser.add_argument('--seed', dest='seed', type=decimal.Decimal,
                        default=decimal.Decimal(time.time()),
                        help='rerun an experiment with a given random seed')
    parser.add_argument('--src_fraction', dest='src_frac', type=float,
                        required=True, help='fraction of nodes that are sources')
    parser.add_argument('--link_fraction', dest='link_frac', type=float,
                        required=True, help='fraction of links that we flip the weight of')
    parser.add_argument('--policies', dest='policies', type=int, default=30,
                        help='maximum number of policies to add')
    parser.add_argument('--mboxes', dest='mboxes', type=int, default=10,
                        help='maximum number of mboxes to add')
    parser.add_argument('--min_length', dest='min_len', type=int, default=4,
                        help='minimum distance from sources to dst')
    parser.add_argument('--strategy', dest='strategy', default='centrality',
                        choices=PolicyRegistry.all())
    parser.add_argument('--stop_and_trace', dest='stop_and_trace', default=False,
                        type=bool, help='on error, stop and spawn a debugger')
    parser.add_argument('--vmodule', dest='vmodule', default='', type=str,
                        help='configure loggers (logger=level,logger2=...)')

    args = parser.parse_args()
    log.critical('args: %s', ' '.join(
        ['--%s=%s' %(k, v) for k, v in vars(args).iteritems()]))
    # Override log levels if we've been told so
    for arg in args.vmodule.split(','):
        if '=' in arg:
            (logger, level) = arg.split('=')
            logging.getLogger(logger).setLevel(level)
    random.seed(args.seed)
    init_graph = convtools.get_networkx_from_ntf(args.topology)
    fin_graph = convtools.get_perturbated_graph(
        init_graph, int(len(init_graph.edges())*args.link_frac))
    dst = random.choice(init_graph.nodes())
    init_dag = md.shortest_paths_dag(init_graph, dst)
    fin_dag = md.shortest_paths_dag(fin_graph, dst)
    if (
            (float(len(init_dag.nodes())) / len(init_graph.nodes()) < 0.5) and
            not nx.is_connected(init_graph)):
        log.critical('Disconnected topology, dst %s yields a connected '
                     'component with only %d nodes, less than half the '
                     'initial size of %d',
                     dst, len(init_dag.nodes()), len(init_graph.nodes()))
        return
    g = flip.Graph(init_dag, fin_dag, dst=dst,
                   srcs=random.sample(
                       init_dag.nodes(),
                       int(math.floor(len(init_dag.nodes())*args.src_frac))))

    # Drop the sources that are too close to the destination.
    log.debug('Sources: %s', g.srcs)
    for src in g.srcs[:]:
        if src == g.dst:
            g.srcs.remove(src)
            continue
        for dag in (init_dag, fin_dag):
            for p in nx.all_simple_paths(dag, src, g.dst):
                if len(p) <= args.min_len and src in g.srcs:
                    log.debug('%s is too close to %s, removing', src, g.dst)
                    g.srcs.remove(src)
                    break

    if args.dump_output_dir:
        # We need to dump
        #  - the initial subgraph induced by the sources, with weights
        #  - the final subgraph induced by the sources, with weights
        #  - the choice of the destination
        #  - the number of nodes that don't change nexthop
        try:
            os.mkdir(args.dump_output_dir)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise e
        reachable = set()
        for dag in [init_dag, fin_dag]:
            for src in g.srcs:
                reachable |= nx.descendants(dag, src)
        for graph, pathname in [(init_graph, 'initial.ntf'),
                                (fin_graph, 'final.ntf')]:
            convtools.output_networkx_to_ntf(
                graph.subgraph(reachable),
                os.path.join(args.dump_output_dir, pathname))
        with open(os.path.join(args.dump_output_dir, 'dst.txt'), 'w') as f:
            f.write('%s\n' % g.dst)

    generator = PolicyRegistry.get(args.strategy)(g, args.policies, args.mboxes)
    generator.add_policies()
    try:
        migration = flip.compute_sequence(graph=generator.graph,
                                          duplicate=args.duplicate)
        migration.verify()
        log.info(print_dag_stats(init_dag, fin_dag, g))
    except Exception as e:
        if args.stop_and_trace:
            pdb.set_trace()
        info = sys.exc_info()
        raise info[0], info[1], info[2]


if __name__ == '__main__':
    main()
