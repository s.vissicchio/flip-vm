#!/bin/bash

SRC=0.1
LINK=0.8
POLICIES=30
MBOXES=10
RADIUS=4
STRATEGY="equivalence"

DATA_DIR="../data/rocketfuel/backbones"
DEBUG=
NPROC=4
NEXP=10

i=1
while [ $i -le ${NEXP} ]; do
    for AS in ${DATA_DIR}/????; do
        AS=${AS##*/}
        RES_DIR="../results/${AS}"
        ${DEBUG} mkdir -p "${RES_DIR}"
        BASENAME="${RES_DIR}/i=${i}_src=${SRC}_lnk=${LINK}_pol=${POLICIES}_mbx=${MBOXES}_radius=${RADIUS}_strategy=${STRATEGY}"
        COMPARISON_DIR=$(mktemp -d)
        sem -j${NPROC} "${DEBUG} /usr/bin/time -v -o ${BASENAME}.time \
            python2.7 run_flip.py \
                --gpia_output_dir ${COMPARISON_DIR} \
                --link_fraction ${LINK} \
                --mboxes ${MBOXES} \
                --min_length ${RADIUS} \
                --policies ${POLICIES} \
                --src_fraction ${SRC} \
                --strategy ${STRATEGY} \
                --topology ${DATA_DIR}/${AS}/weights.intra \
                > ${BASENAME}.txt 2>&1 && \
            python2.7 gpia/run_gpia.py \
                ${COMPARISON_DIR}/initial.ntf \
                ${COMPARISON_DIR}/final.ntf \
                ira2ira \
                -l ${COMPARISON_DIR}/dst.txt \
                > ${BASENAME}.gpia 2>&1 && \
            rm -Rf ${COMPARISON_DIR}"
        ((i+=1))
    done
done
sem --wait
