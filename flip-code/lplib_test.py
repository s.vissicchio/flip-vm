import unittest, os
from lplib import *


class MockConstraint(ConvertibleToILPConstraint):
    """Wraps a pair of lists into an object."""
    def __init__(self, something):
        if isinstance(something, ConvertibleToILPConstraint):
            self.raw = something.to_list()
        elif type(something) is tuple:
            self.raw = [something]
        else:
            raise RuntimeError('called with %s' % something)

    def to_list(self):
        return self.raw


def _mock(list_of_tuples):
    return [MockConstraint(t) for t in list_of_tuples]


class ReadyConstraintLists:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if (cls._instance is None):
            cls._instance = super.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        self.feasibleclist1 = _mock([(['a'],['b']),(['b'],['c','d'])])
        self.infeasibleclist1 = _mock([(['a'],['b']),(['b'],['a'])])
        self.infeasibleclist2 = _mock(
            [(['a'],['b']),(['c'],['d']),(['b'],['a']),(['d'],['c'])])
        

class TestCase(unittest.TestCase):
    def setUp(self):
        self.clists = ReadyConstraintLists()
    
    def testGurobiModel(self):
        solver = Solver(self.clists.feasibleclist1,['a','b','c','d'])
        solver.translate_to_lp()
        model = solver.gurobi_model
        self.assertTrue(model != None)

class InfeasibleTestCase(unittest.TestCase):
    def setUp(self):
        self.clists = ReadyConstraintLists()
    
    def testSingleConstraint(self):
        solver = Solver(self.clists.infeasibleclist1,['a','b'])
        iis = solver.compute_IIS()
        self.assertEqual(2, len(iis))
        self.assertItemsEqual(iis, self.clists.infeasibleclist1)

    def testRedundantConstraint(self):
        clist = _mock([(['a'],['b']),(['c'],['d']),(['b'],['a'])])
        solver = Solver(clist, ['a','b','c','d'])
        iis = solver.compute_IIS()
        self.assertEqual(2, len(iis))
        self.assertItemsEqual(
            [str(x.to_list()) for x in _mock([(['a'],['b']),(['b'],['a'])])],
            [str(x.to_list()) for x in iis])

    def testMultipleUnsatConstraints(self):
        clist = self.clists.infeasibleclist2[:]
        solver = Solver(clist, ['a','b','c','d'])
        iis = solver.compute_IIS()
        self.assertEqual(2, len(iis))
        clist.remove(iis[0])
        iis2 = solver.compute_IIS()
        self.assertNotEqual(iis, iis2)
        self.assertEqual(2, len(iis2))
        alliis = iis + iis2
        self.assertItemsEqual(
            [str(x.to_list()) for x in self.clists.infeasibleclist2],
            [str(x.to_list()) for x in alliis])


if __name__ == '__main__':
    unittest.main()
