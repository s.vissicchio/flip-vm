import flip
import inspect
import logging
import mock
import networkx as nx
import pprint
import unittest
import verify


PPRINT = pprint.PrettyPrinter(indent=2)
logging.getLogger(flip.__name__).setLevel(-10)
logging.getLogger(verify.__name__).setLevel(logging.DEBUG)


def _get_fn_name():
    return inspect.stack()[1][3]


class Gadgets():
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        self._setUpImpossibleGadgets()
        self._setUpImpossiblePolicies()
        self._setUpRedCycleGadget()
        self._setUpEstuary()
        self._setUpPyramid()
        self._setUpLongDiamond()
        self._setUpSpider()
        self._setUpTrickyPred()
        self._setUpSampleConstraints()
        self._setUpColorBlind()

    def _setUpImpossibleGadgets(self):
        conext_init = nx.DiGraph()
        conext_init.add_path(['u', 'z', 'v', 'w'])
        conext_fin = nx.DiGraph()
        conext_fin.add_path(['u', 'v', 'z', 'w'])
        conext_init2 = conext_init.copy()
        conext_fin2 = conext_fin.copy()
        conext_fin2.add_edge('u','i')
        conext_fin2.add_edge('i','w')
        self.conext = flip.Graph(conext_init, conext_fin,
                                 dst='w', srcs=['u'])
        self.conext_blackhole = flip.Graph(conext_init2, conext_fin2,
                                           dst='w', srcs=['u'])

    def _setUpRedCycleGadget(self):
        redcycle_init = nx.DiGraph()
        redcycle_init.add_edges_from([('s', 'a'), ('a', 'x'), ('x', 'z'), 
                                      ('b', 'd'), ('d', 'c'), ('c', 'z')])
        redcycle_fin = nx.DiGraph()
        redcycle_fin.add_edges_from([('s', 'c'), ('c', 'a'), ('a', 'b'),
                                     ('b', 'x'), ('x', 'z'), ('d', 'z')])
        self.redcycle = flip.Graph(redcycle_init, redcycle_fin,
                                   dst='z', srcs=['s'])

    def _setUpImpossiblePolicies(self):
        init = nx.DiGraph()
        init.add_edges_from([('s', '1'), ('1', '3'), ('3', '5'), ('5', 'd')])
        fin = nx.DiGraph()
        fin.add_edges_from([('s', '2'), ('2', '3'), ('3', '4'), ('4', 'd')])
        self.impossible_policies = flip.Graph(
            init, fin, dst='d', srcs=['s'],
            subpaths=[['1', '3', '5'], ['2', '3', '4']])

    def _setUpEstuary(self):
        est_init = nx.DiGraph()
        est_init.add_edges_from([
            ('s1', 'y1'), ('y1', 'm1'), ('m1', 'y2'), ('y2', 'm2'),
            ('s2', 'm2'), ('x2', 'm2'), ('m2', 'x3'), ('m2', 'z'), ('x3', 'z')])
        est_fin = nx.DiGraph()
        est_fin.add_edges_from([
            ('s1', 'y1'), ('y1', 'm1'), ('m1', 'x3'), ('y2', 'm2'),
            ('s2', 'x2'), ('x2', 'm1'), ('m2', 'x3'), ('m2', 'z'), ('x3', 'z')])
        self.est = flip.Graph(est_init, est_fin, dst='z', srcs=['s1', 's2'])
        

    def _setUpPyramid(self):
        pyramid_init = nx.DiGraph()
        pyramid_init.add_edges_from([('a','D'),('b','D')])
        pyramid_init.add_edges_from([('c','f'),('f','a'),('d','a'),('d','b'),('e','b')])
        pyramid_init.add_edges_from([('s1','c'),('s2','c'),('s2','e'),('s3','d'),('s3','e')])
        pyramid_fin = pyramid_init.copy()
        pyramid_init.remove_edges_from([('s2', 'c'), ('s3', 'd')])
        pyramid_fin.remove_node('e')
        self.pyramid = flip.Graph(pyramid_init, pyramid_fin,
                             dst='D', srcs=['s1', 's2'])

    def _setUpSpider(self):
        spider_init = nx.DiGraph()
        spider_init.add_edges_from([('s','m'),('x1','m'),('x2','m'),('x3','z'),('m','z')])
        spider_fin = nx.DiGraph()
        spider_fin.add_edges_from([('s','x3'),('x3','m'),('x1','x2'),('x2','z'),('m','z')])
        self.spider = flip.Graph(spider_init, spider_fin,
                                 dst='z', srcs=['s'])

    def _setUpLongDiamond(self):
        self.ldiamond = nx.DiGraph()
        self.ldiamond.add_edges_from([('a','b'),('a','c'),('b','d'),('c','d'),('c','e'),('b','e'),('d','z'),('e','z')])

    def _setUpTrickyPred(self):
        trickypred_init = nx.DiGraph()
        trickypred_init.add_edges_from([('s','fw'),('fw','z'),('p1','p2'),('p1','x'),('p2','p3'),('p3','x'),('x','z')])
        trickypred_fin = nx.DiGraph()
        trickypred_fin.add_edges_from([('s','p4'),('p4','p3'),('p3','fw'),('fw','z'),('p1','x'),('p2','x'),('x','fw')])
        self.trickypred = flip.Graph(trickypred_init, trickypred_fin,
                                     dst='z', srcs=['s'])

    def _setUpColorBlind(self):
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'c', 'z'])
        i.add_path(['s2', 'a', 'd', 'e', 'z'])
        self.blind = flip.Graph(i, i, dst='z', srcs=['s1', 's2'])

    def _setUpSampleConstraints(self):        
        empty = nx.DiGraph()
        empty.add_nodes_from('abcd')
        self.empty = flip.Graph(empty, empty, dst='d')
        self.empty.initialize()
        self.lcon1 = flip.LoopConstraint(before_grp=['a','b'],
                                         after_grp=['c','d'])
        self.lcon2 = flip.LoopConstraint(before_grp=['a','b'], after_grp=['c'])
        self.lcon3 = flip.LoopConstraint(before_grp=['b'], after_grp=['a'])
        self.lcon4 = flip.LoopConstraint(before_grp=['c'], after_grp=['a','b'])
        self.lcon5 = flip.LoopConstraint(before_grp=['a','c'],
                                         after_grp=['b','d'])
        self.lcon6 = flip.LoopConstraint(before_grp=['b'], after_grp=['c'])
        self.lcon7 = flip.LoopConstraint(before_grp=['a'], after_grp=['b'])

        self.pcon1 = flip.PolicyConstraint(before_grp=['a'], after_grp=['b'])
        self.pcon2 = flip.PolicyConstraint(before_grp=['b'],
                                           after_grp=['c','d'])
        self.pcon3 = flip.PolicyConstraint(before_grp=['b'], after_grp=['a'])

        self.mcon1 = flip.MatchingConstraint()
        self.mcon1.node = 'a'
        self.mcon2 = flip.MatchingConstraint()
        self.mcon2.node = 'b'


class GraphTestCase(unittest.TestCase):
    def setUp(self):
        self.gadgets = Gadgets()

    def _testMergeDAGsWithSubpaths(self, name, graph,
                                   exp_i, exp_f, subpaths):
        """Inner helper function."""
        graph = graph.copy()
        graph.subpaths = subpaths
        graph.initialize()
        actual = dict([(n, graph.initial.node[n]['color']) for n in exp_i])
        self.assertEqual(exp_i, actual,
                         '[%s] - expected (init) \n%s\n, found \n%s\n' % (
                         name, PPRINT.pformat(exp_i), PPRINT.pformat(actual)))
        actual = dict([(n, graph.final.node[n]['color']) for n in exp_f])
        self.assertEqual(exp_f, actual,
                         '[%s] - expected (fin) \n%s\n, found \n%s\n' % (
                         name, PPRINT.pformat(exp_f), PPRINT.pformat(actual)))

    def testMergeDAGsWithColors(self):
        # Actual test case begins here
        for test, graph, exp_i, exp_f, subpaths in [
                ('Estuary',  self.gadgets.est, {'s1': 'green',
                                                's2': 'green',
                                                'm2': 'green',
                                                'x2': 'yellow',
                                                'x3': 'white',
                                                'y1': 'green',
                                                'y2': 'green',
                                                'z': 'white',
                                               },
                                               {'s1': 'green',
                                                's2': 'green',
                                                'm2': 'yellow',
                                                'x2': 'green',
                                                'x3': 'white',
                                                'y1': 'green',
                                                'y2': 'yellow',
                                                'z': 'white',
                                               }, [['m1'], ['m2']]),
                ('OneLoop',  self.gadgets.conext, {'u': 'green',
                                                   'v': 'white',
                                                   'z': 'green',
                                                   'w': 'white',
                                                  },
                                                  {'u': 'green',
                                                   'v': 'green',
                                                   'z': 'white',
                                                   'w': 'white',
                                                  }, [['v', 'z'], ['z', 'v']]),
                ('Spider',  self.gadgets.spider, {'s': 'green',
                                                  'm': 'green',
                                                  'x1': 'yellow',
                                                  'x2': 'yellow',
                                                  'x3': 'yellow',
                                                  'z': 'white',
                                                 },
                                                 {'s': 'green',
                                                  'm': 'green',
                                                  'x1': 'yellow',
                                                  'x2': 'yellow',
                                                  'x3': 'green',
                                                  'z': 'white',
                                                 }, [['m']]),
                ('RedCyle', self.gadgets.redcycle, {'s': 'green',
                                                    'a': 'green',
                                                    'b': 'yellow',
                                                    'c': 'yellow',
                                                    'd': 'yellow',
                                                    'x': 'green',
                                                    'z': 'white',
                                                   },
                                                   {'s': 'green',
                                                    'a': 'green',
                                                    'b': 'green',
                                                    'c': 'green',
                                                    'd': 'yellow',
                                                    'x': 'green',
                                                    'z': 'white',
                                                   }, [['x']]),
                ('Tricky', self.gadgets.trickypred, {'s': 'green',
                                                     'fw': 'green',
                                                     'p1': 'yellow',
                                                     'p2': 'yellow',
                                                     'p3': 'yellow',
                                                     'z': 'white',
                                                    },
                                                    {'s': 'green',
                                                     'fw': 'green',
                                                     'p1': 'yellow',
                                                     'p2': 'yellow',
                                                     'p3': 'green',
                                                     'p4': 'green',
                                                     'z': 'white',
                                                    }, [['fw']]),
                ('ColorBlind (green)',
                # Here s1->a->d is superfluous and should be discarded
                 self.gadgets.blind, {'s1': 'green',
                                      's2': 'green',
                                      'a': 'green',
                                      'b': 'cyan',
                                      'c': 'white',
                                      'd': 'green',
                                      'e': 'cyan',
                                      'z': 'white',
                                     }, {}, [['a', 'b', 'c'],
                                             ['d', 'e', 'z'],
                                             ['s1', 'a', 'd']]),
                ('ColorBlind (overlap)',
                # Here s1->a->d is superfluous and should be discarded,
                # but a->d->e cannot be discarded, so d cannot be green.
                 self.gadgets.blind, {'s1': 'green',
                                      's2': 'green',
                                      'a': 'green',
                                      'b': 'cyan',
                                      'c': 'white',
                                      'd': 'cyan',
                                      'e': 'cyan',
                                      'z': 'white',
                                     }, {}, [['a', 'b', 'c'],
                                             ['d', 'e', 'z'],
                                             ['a', 'd', 'e'],
                                             ['s1', 'a', 'd']]),
                ('ColorBlind (cyan)',
                 self.gadgets.blind, {'s1': 'green',
                                      's2': 'green',
                                      'a': 'cyan',
                                      'b': 'cyan',
                                      'c': 'white',
                                      'd': 'white',
                                      'e': 'white',
                                      'z': 'white',
                                     }, {}, [['a', 'b', 'c'],
                                             ['s1', 'a', 'd'],
                                             ['s2', 'a', 'd']]),
                ('Pyramid', self.gadgets.pyramid, {'s1': 'green',
                                                   's2': 'green',
                                                   's3': 'yellow',
                                                   'a': 'white',
                                                   'b': 'white',
                                                   'c': 'green',
                                                   'd': 'yellow',
                                                   'e': 'green',
                                                   'f': 'cyan',
                                                   'D': 'white',
                                                  },
                                                  {'s1': 'green',
                                                   's2': 'green',
                                                   's3': 'yellow',
                                                   'a': 'white',
                                                   'b': 'yellow',
                                                   'c': 'green',
                                                   'd': 'yellow',
                                                   'f': 'cyan',
                                                   'D': 'white',
                                                  }, [['c', 'f', 'a'], ['e']]),
                ]:
            self._testMergeDAGsWithSubpaths(
                _get_fn_name() + test, graph, exp_i, exp_f, subpaths)

    def testAllSuccessorsForGreens(self):
        """All successors must be green to promote a node to green."""
        dag = nx.DiGraph()
        dag.add_path(['s1', 'a', 'b', 'c', 'z'])
        dag.add_path(['a', 'd', 'c', 'z'])
        expected = {
            's1': 'green',
            'a': 'cyan',
            'b': 'cyan',
            'c': 'cyan',
            'z': 'white',
        }
        policies = [['s1', 'a', 'b'], ['d', 'c', 'z']]
        g = flip.Graph(dag, dag, srcs=['s1'], dst='z')
        self._testMergeDAGsWithSubpaths(
            _get_fn_name(), g, expected, expected, policies)


class NaiveMinimizationStrategyTestCase(unittest.TestCase):
    def setUp(self):
        self.gadgets = Gadgets()
        self.storage = flip.ConstraintStorage(self.gadgets.empty)
        self.storage.optimization_strategy = (
            flip.NaiveMinimizationStrategy())

    def testOptimizationWithoutAlternatives(self):
        Lcons = [self.gadgets.lcon1, self.gadgets.lcon5]
        Pcons = [self.gadgets.pcon1]
        self.storage._add_constraints(Lcons, 'L')
        self.storage._add_constraints(Pcons, 'P')
        new_storage = self.storage.select_best_alternatives()
        self.assertItemsEqual(Lcons, new_storage.get_group('L'))
        self.assertItemsEqual(Pcons, new_storage.get_group('P'))
        self.assertFalse(new_storage.get_group('M'))

    def testTwoUnmergeableConstraints(self):
        Lcons = [self.gadgets.lcon3]
        Pcons = [self.gadgets.pcon1]
        Mcons = [self.gadgets.mcon1]
        self.storage._add_constraints(Lcons, 'L')
        self.storage._add_constraints(Pcons, 'P')
        self.storage._add_constraints(Mcons, 'M')
        self.storage._add_alternative_constraints(Pcons[0], Mcons)
        self.storage._add_alternative_constraints(Lcons[0], Mcons)
        new_storage = self.storage.select_best_alternatives()
        self.assertFalse(new_storage.get_group('L'))
        self.assertFalse(new_storage.get_group('P'))
        self.assertItemsEqual(Mcons, new_storage.get_group('M'))

    def testThreeRedundantUnmergeableConstraints(self):
        Lcons = [self.gadgets.lcon3]
        Pcons = [self.gadgets.pcon1,self.gadgets.pcon3]
        Mcons = [self.gadgets.mcon1]
        self.storage._add_constraints(Lcons, 'L')
        self.storage._add_constraints(Pcons, 'P')
        self.storage._add_constraints(Mcons, 'M')
        self.storage._add_alternative_constraints(self.gadgets.pcon1, Mcons)
        self.storage._add_alternative_constraints(self.gadgets.pcon3, Mcons)
        self.storage._add_alternative_constraints(self.gadgets.lcon3, Mcons)
        new_storage = self.storage.select_best_alternatives()
        self.assertFalse(new_storage.get_group('L'))
        self.assertFalse(new_storage.get_group('P'))
        self.assertItemsEqual(Mcons, new_storage.get_group('M'))

    def testMergeableLconstraints(self):
        Lcons = [self.gadgets.lcon3, self.gadgets.lcon4, self.gadgets.lcon6]
        Pcons = []
        Mcons = [self.gadgets.mcon2]
        self.storage._add_constraints(Lcons, 'L')
        self.storage._add_constraints(Pcons, 'P')
        self.storage._add_constraints(Mcons, 'M')
        self.storage._add_alternative_constraints(self.gadgets.lcon6, Mcons)
        self.storage._add_alternative_constraints(self.gadgets.lcon4, Mcons)
        self.storage._add_alternative_constraints(self.gadgets.lcon3, Mcons)
        new_storage = self.storage.select_best_alternatives()
        self.assertItemsEqual(Lcons, new_storage.get_group('L'))
        self.assertFalse(new_storage.get_group('P'))
        self.assertFalse(new_storage.get_group('M'))
 
    def testUnmergeableLconstraints(self):
        Lcons = [self.gadgets.lcon7, self.gadgets.lcon4, self.gadgets.lcon6]
        Pcons = []
        Mcons = [self.gadgets.mcon2]
        self.storage._add_constraints(Lcons, 'L')
        self.storage._add_constraints(Pcons, 'P')
        self.storage._add_constraints(Mcons, 'M')
        self.storage._add_alternative_constraints(self.gadgets.lcon6, Mcons)
        self.storage._add_alternative_constraints(self.gadgets.lcon4, Mcons)
        self.storage._add_alternative_constraints(self.gadgets.lcon7, Mcons)
        new_storage = self.storage.select_best_alternatives()
        self.assertFalse(new_storage.get_group('L'))
        self.assertFalse(new_storage.get_group('P'))
        self.assertItemsEqual(Mcons, new_storage.get_group('M'))


class OrderingConstraintTestCase(unittest.TestCase):
    def testAnyOrderingConstraintNoDuplicates(self):
        c = flip.LoopConstraint(['node'], ['node'])
        self.assertItemsEqual([], c.to_list())
        c = flip.LoopConstraint(['node1', 'node2'], ['node1'])
        self.assertItemsEqual([(['node1', 'node2'], ['node1'])], c.to_list())

    def testAllOrderingConstraintNoDuplicates(self):
        c = flip.PolicyConstraint(['node1', 'node2'], ['node1', 'node3'])
        self.assertItemsEqual([(['node1'], ['node3']),
                               (['node2'], ['node1']),
                               (['node2'], ['node3'])], c.to_list())

    def testReplaceAnyOrderingConstraint(self):
        c = flip.LoopConstraint(['node1', 'node2'], ['node3', 'node4'])
        replacement = c.replace('node2', ['node2alt1', 'node2alt2'])
        self.assertItemsEqual([
            flip.LoopConstraint(['node1', 'node2alt1'],
                                ['node3', 'node4']),
            flip.LoopConstraint(['node1', 'node2alt2'],
                                ['node3', 'node4'])],
            replacement)

    def testReplaceAllOrderingConstraint(self):
        c = flip.PolicyConstraint(['node1', 'node2'], ['node3', 'node4'])
        replacement = c.replace('node2', ['node2alt1', 'node2alt2'])
        self.assertItemsEqual([
            flip.PolicyConstraint(['node1', 'node2alt1', 'node2alt2'],
                                  ['node3', 'node4'])],
            replacement)


class ConstraintStorageTestCase(unittest.TestCase):
    def setUp(self):
        self.gadgets = Gadgets()

    def testExtractSimpleBlackholeConstraint(self):
        g1 = nx.DiGraph()
        g1.add_edges_from([('s', 'd')])
        g2 = nx.DiGraph()
        g2.add_edges_from([('s', 't'), ('t', 'd')])
        g = flip.Graph(g1, g2, dst='d', srcs=['s'])
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        expected = flip.BlackholeConstraint(before_grp=['t'], after_grp=['s'])
        self.assertItemsEqual([str(expected)],
                              [str(x) for x in storage.get_group('B')])
        for group in ['M', 'L', 'P']:
            self.assertFalse(storage.get_group(group))

    def testExtractSimpleLoopConstraint(self):
        storage = flip.ConstraintStorage(graph=self.gadgets.conext)
        storage.extract()
        expected = flip.LoopConstraint(before_grp=['z'], after_grp=['v'])
        self.assertItemsEqual([str(expected)],
                              [str(x) for x in storage.get_group('L')])
        self.assertFalse(storage.get_group('P'))

    def testLoopNoTaggingOnTransitNodes(self):
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'c', 'd'])
        f = nx.DiGraph()
        f.add_path(['s1', 'b', 'c', 'a', 'd'])
        g = flip.Graph(i, f, 'd', srcs=['s1'], subpaths=[
            ['a', 'b', 'c'], ['b', 'c', 'a']])
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        expected1 = flip.MatchingConstraint('c', set(['a']), set(['s1']))
        expected2 = flip.MatchingConstraint('a', set(['s1']), set(['c']))
        self.assertItemsEqual([str(expected1), str(expected2)],
                              [str(x) for x in storage.get_group('M', True)])

    def testOneFirewallTraversalConstraint(self):
        idag = self.gadgets.ldiamond.copy()
        idag.remove_edges_from([('a','b'), ('c','e')])
        fdag = self.gadgets.ldiamond.copy()
        fdag.remove_edges_from([('a','c'), ('b','e')])
        g = flip.Graph(idag, fdag, 'z', srcs=['a'], subpaths=[['d']])
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        self.assertFalse(storage.get_group('L'))
        policy_cons = storage.get_group('P')
        self.assertEqual(2, len(policy_cons))
        for c in policy_cons:
            self.assertEqual('ALL', c.kind)
            self.assertTrue((c.before_grp == ['a'] and c.after_grp == ['c']) or
                            (c.before_grp == ['b'] and c.after_grp == ['a']))

    def testSkipUnreachableLoop(self):
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'c', 'z'])
        i.add_path(['d', 'e', 'z'])  # useless nodes
        f = nx.DiGraph()
        f.add_path(['s1', 'a', 'f', 'z'])
        f.add_path(['e', 'd', 'z'])
        g = flip.Graph(i, f, dst='z', srcs=['s1'], subpaths=[
            ['a', 'b', 'c'], ['a', 'f', 'z']])
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        self.assertItemsEqual([], storage.get_loop_constraints())
 
    def testRedCycleGadget(self):
        g = self.gadgets.redcycle.copy()
        g.subpaths = [['x']]
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        expected = flip.LoopConstraint(before_grp=['b', 'd'],
                                       after_grp=['a', 'c'])
        self.assertEqual([str(expected)],
                         [str(x) for x in storage.get_group('L')])
        policy_cons = storage.get_group('P')
        self.assertEqual(2, len(policy_cons))
        for c in policy_cons:
            self.assertEqual('ALL', c.kind)
            self.assertTrue((c.before_grp == ['b'] and c.after_grp == ['a']) or
                            (c.before_grp == ['c'] and c.after_grp == ['s']))        

    def testPolicyAndLoopConstraint(self):
        idag = self.gadgets.ldiamond.copy()
        idag.add_edges_from([('s1','a'),('s2','a')])
        idag.remove_edges_from([('a','b'),('c','e')])
        fdag = self.gadgets.ldiamond.copy()
        fdag.add_edges_from([('s1','d'),('s2','d'),('a','s1')])
        fdag.remove_edges_from([('b','e'),('a','c')])
        g = flip.Graph(idag, fdag, 'z', srcs=['s1', 's2'], subpaths=[['d']])
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        policy_cons = storage.get_group('P')
        expected = flip.LoopConstraint(before_grp=['s1'], after_grp=['a'])
        self.assertEqual([str(expected)],
                         [str(x) for x in storage.get_group('L')])
        self.assertEqual(2, len(policy_cons))
        self.assertItemsEqual(
            [flip.PolicyConstraint(['s1', 's2'], ['a']),
             flip.PolicyConstraint(['a'], ['c'])],
            policy_cons)
        
    def testTrickyPredGadget(self):
        g = self.gadgets.trickypred.copy()
        g.subpaths = [['fw']]
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        expectedP = flip.PolicyConstraint(before_grp=['p3'], after_grp=['s'])
        expectedB = flip.BlackholeConstraint(
            after_grp=g.srcs, before_grp=['p4'])
        self.assertFalse(storage.get_group('L'))
        self.assertEqual([str(expectedP)],
                         [str(x) for x in storage.get_group('P')])
        self.assertEqual([str(expectedB)],
                         [str(x) for x in storage.get_group('B')])

    def testEstuary(self):
        g = self.gadgets.est.copy()
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        self.assertFalse(storage.get_group('L'))
        self.assertFalse(storage.get_group('P'))

    def testSpider(self):
        """This test proves that yellow nodes are useful."""
        g = self.gadgets.spider.copy()
        g.subpaths = [['m']]
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        expected = flip.PolicyConstraint(before_grp=['x3'], after_grp=g.srcs)
        self.assertFalse(storage.get_group('L'))
        self.assertEqual([str(expected)],
                         [str(x) for x in storage.get_group('P')])

    def testImpossibleGadget(self):
        g = self.gadgets.conext.copy()
        g.subpaths = [['v','z'],['z','v']]
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        expectedL = flip.LoopConstraint(before_grp=['z'], after_grp=['v'])
        p1 = flip.PolicyConstraint(before_grp=['v'], after_grp=['u'])
        p2 = flip.PolicyConstraint(before_grp=['u'], after_grp=['z'])
        m1 = flip.MatchingConstraint(
            node='z', tag_initial=set(['u']), tag_final=set(['v']))
        m2 = flip.MatchingConstraint(
            node='v', tag_initial=set(['z']), tag_final=set(['u']))
        self.assertEqual([str(expectedL)],
                         [str(x) for x in storage.get_group('L')])
        self.assertItemsEqual([str(x) for x in [p1, p2]],
                              [str(x) for x in storage.get_group('P')])
        self.assertEqual(set([str(x) for x in [m1, m2]]),
                         set([str(x) for x in storage.get_group(
                             'M', also_inactive=True)]))

    def testLoopConstraintExcludesTrivialNodes(self):
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'c', 'd', 'z'])
        i.add_edge('e', 'b')
        f = nx.DiGraph()
        f.add_path(['s1', 'a', 'b', 'c', 'z'])
        f.add_path(['d', 'e', 'b'])
        g = flip.Graph(i, f, dst='z', srcs=['s1'], subpaths=[['b']])
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        expected = flip.LoopConstraint(before_grp=['c'], after_grp=['d'])
        self.assertItemsEqual([str(expected)],
                              [str(x) for x in storage.get_loop_constraints()])

    def testMultiLoop(self):
        """Multiple nested loops should generate multiple constraints.

        In this test we have three loops, all reachable from the source:
        (-> is initial, -- is final)
         - a -> b -> c -> d -> e -- b
         - a -> b -> f -- e -- b
         - a -> b -> c -- f -- e -- b

        TODO: in addition to checking the number of constraints, we should
        also make sure that the constraint are restrictive.
        """
        i = nx.DiGraph()
        i.add_path(['a', 'b', 'c', 'd', 'e', 'z'])
        i.add_path(['a', 'b', 'f', 'z'])
        f = nx.DiGraph()
        f.add_path(['a', 'b', 'z'])
        f.add_path(['c', 'f', 'e', 'z'])
        f.add_edge('d', 'e')
        f.add_edge('e', 'b')
        g = flip.Graph(i, f, dst='z', srcs=['a'], subpaths=[
            ['b']])
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        self.assertGreaterEqual(len(storage.get_loop_constraints()), 3)
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testCrucialPredecessorNotEmpty(self):
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'z'])
        i.add_path(['c', 'd', 'z'])
        i.add_edge('s2', 's1')
        f = nx.DiGraph()
        f.add_path(['s1', 'a', 'c', 'z'])
        f.add_path(['s2', 's1'])
        g = flip.Graph(i, f, dst='z', srcs=['s1', 's2'], subpaths=[
            ['a', 'c', 'z'], ['s1', 'a', 'b']])
        storage = flip.ConstraintStorage(graph=g)
        storage.extract()
        expected = flip.MatchingConstraint('a', set(['s1']), set(['s1']))
        self.assertIn(expected, storage.get_group('M', True))

    def testCrucialPredecessorExpandAllBranches(self):
        """A node can be crucial for one branch but not for another.

        Here we make sure that when this happens we continue visiting that
        node's predecessors and we don't stop just because a branch is done.
        """
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'z'])
        i.add_path(['c', 'b', 'z'])
        i.add_path(['d', 'b', 'z'])
        i.add_path(['e', 'b', 'z'])
        f = nx.DiGraph()
        f.add_path(['s1', 'a', 'b', 'z'])
        f.add_path(['s1', 'a', 'c', 'b', 'z'])
        f.add_path(['s1', 'a', 'd', 'b', 'z'])
        f.add_path(['s1', 'a', 'e', 'b', 'z'])
        g = flip.Graph(i, f, dst='z', srcs=['s1'])
        self.assertItemsEqual(
            ['s1', 'a'],
            g.find_crucial_predecessors('b', g.final)[0])

    def testCrucialPredecessorNonLeaf(self):
        """Test that we correctly identify all non-leaf crucial predecessors."""
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'z'])
        i.add_path(['c', 'b', 'z'])
        i.add_path(['d', 'b', 'z'])
        i.add_path(['e', 'b', 'z'])
        f = nx.DiGraph()
        f.add_path(['s1', 'a', 'b', 'z'])
        f.add_path(['s1', 'a', 'c', 'b', 'z'])
        f.add_path(['s1', 'a', 'd', 'b', 'z'])
        f.add_path(['s1', 'a', 'e', 'b', 'z'])
        g = flip.Graph(i, f, dst='z', srcs=['s1'])
        actual, non_leaf = g.find_crucial_predecessors('b', g.final)
        self.assertItemsEqual(['s1', 'a'], actual)
        self.assertItemsEqual(['a'], non_leaf)

    def testReplaceConstraint(self):
        storage = flip.ConstraintStorage()
        p1 = flip.PolicyConstraint(['a'], ['b'])
        m1 = flip.MatchingConstraint('c', ['d'], ['e'])
        new_p1 = flip.PolicyConstraint(['f'], ['g'])
        new_p2 = flip.PolicyConstraint(['h'], ['i'])
        storage.constraints2groups = {m1: 'M', p1: 'P'}
        storage.groups2constraints = {'M': set([m1]), 'P': set([p1])}
        storage.active_constraints.add(p1)
        storage._add_alternative_constraints(p1, [m1])
        storage.replace_constraint(p1, [new_p1, new_p2])
        self.assertItemsEqual([new_p1, new_p2], storage.active_constraints)
        self.assertItemsEqual([new_p1, new_p2], storage.get_alternatives(m1))
        self.assertItemsEqual([m1], storage.get_alternatives(new_p1))
        self.assertItemsEqual([m1], storage.get_alternatives(new_p2))
        self.assertItemsEqual([new_p1, new_p2],
                              storage.get_policy_constraints())

    def testRecursivelySwapConstraints(self):
        """Swapping a constraint should recursively expand matcher nodes.

        Here we swap e --> f before we swap a --> d,e.
        Constraints that used to refer to a should now refer to d,f.
        """
        storage = flip.ConstraintStorage()
        storage.graph = mock.Mock()
        storage.graph.srcs = []
        p1 = flip.PolicyConstraint(['a'], ['b'])
        p2 = flip.PolicyConstraint(['c'], ['e'])
        p3 = flip.PolicyConstraint(['g'], ['a', 'b'])
        m1 = flip.MatchingConstraint('a', ['d'], ['e'])
        m2 = flip.MatchingConstraint('e', ['f'], ['f'])
        storage.constraints2groups = {
            m1: 'M', m2: 'M', p1: 'P', p2: 'P', p3:'P'
        }
        storage.groups2constraints = {
            'M': set([m1, m2]), 'P': set([p1, p2, p3])
        }
        storage._add_alternative_constraints(p1, [m1])
        storage._add_alternative_constraints(p2, [m2])
        storage._initialize_active_constraints()
        storage.swap(m2)
        self.assertItemsEqual([p1, p3, m2], storage.get_active_constraints())
        storage.swap(m1)
        expected_p3 = flip.PolicyConstraint(['g'], ['b', 'd', 'f'])
        self.assertItemsEqual([expected_p3, m1, m2],
                              storage.get_active_constraints())

    def testSwapConstraintsWithSources(self):
        """Swapping a constraint should leave sources alone.

        Here we swap a --> d,e.
        Constraints that used to refer to a should now refer to d,e and to a,
        because a is a source.
        """
        storage = flip.ConstraintStorage()
        storage.graph = mock.Mock()
        storage.graph.srcs = ['a']
        p1 = flip.PolicyConstraint(['a'], ['b'])
        p2 = flip.PolicyConstraint(['g'], ['a', 'b'])
        p3 = flip.PolicyConstraint(['c'], ['a', 'g'])
        m1 = flip.MatchingConstraint('a', ['d'], ['e'])
        m2 = flip.MatchingConstraint('g', ['f'], ['f'])
        storage.constraints2groups = {
            m1: 'M', m2: 'M', p1: 'P', p3:'P'
        }
        storage.groups2constraints = {
            'M': set([m1, m2]), 'P': set([p1, p3])
        }
        storage._add_alternative_constraints(p1, [m1])
        storage._add_alternative_constraints(p2, [m2])
        storage._initialize_active_constraints()
        storage.swap(m2)
        storage.swap(m1)
        expected_p3 = flip.PolicyConstraint(['c'], ['a', 'd', 'e', 'f'])
        self.assertItemsEqual([expected_p3, m1, m2],
                              storage.get_active_constraints())


class CompressGraphTestCase(unittest.TestCase):
    def setUp(self):
        self.gadgets = Gadgets()

    def testSingleCompression(self):
        g = nx.DiGraph()
        g.add_edge(1,2,label='init')
        g.add_edge(3,2,label='fin')
        g.add_edge(2,4,label='both')
        g.add_edge(2,5,label='both')
        g.add_edge(5,6,label='both')
        g.add_edge(5,7,label='init')
        for node in g.nodes():
            g.node[node]['color'] = 'white'
        c = flip.compress_graph(g)
        cnodes = c.nodes()
        cnodes.sort()
        self.assertTrue(2 not in cnodes)
        self.assertTrue(cnodes == [1,3,4,5,6,7])
        self.assertTrue(c.has_edge(1,4) and c.edge[1][4]['label']=='init')
        self.assertTrue(c.has_edge(3,4) and c.edge[3][4]['label']=='fin')
        self.assertTrue(c.has_edge(1,5) and c.edge[1][5]['label']=='init')
        self.assertTrue(c.has_edge(3,5) and c.edge[3][5]['label']=='fin')

    def testCompressionWithCycle(self):
        g = nx.DiGraph()
        g.add_edge(1,2,label='init')
        g.add_edge(2,3,label='fin')
        g.add_edge(3,1,label='both')
        g.add_edge(2,5,label='both')
        g.add_edge(5,6,label='both')
        g.add_edge(5,7,label='init')
        for node in g.nodes():
            g.node[node]['color'] = 'white'
        c = flip.compress_graph(g)
        cnodes = c.nodes()
        cnodes.sort()    
        self.assertTrue(3 not in cnodes)
        self.assertTrue(cnodes == [1,2,5,6,7])


class EndToEndTestCase(unittest.TestCase):
    def setUp(self):
        self.gadgets = Gadgets()

    def testImpossibleGadget(self):
        g = self.gadgets.conext.copy()
        g.subpaths = [['v', 'z'], ['z', 'v']]
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testImpossibleGadgetWithBlackhole(self):
        g = self.gadgets.conext_blackhole.copy()
        g.subpaths = [['v', 'z'], ['z', 'v']]
        for dup in (False, True):
            with self.assertRaises(flip.PreconditionFailedError):
                flip.compute_sequence(graph=g, duplicate=dup)

    def testImpossibleGadgetWithBlackholeFlexiblePolicies(self):
        g = self.gadgets.conext_blackhole.copy()
        g.subpaths = [['v', 'z'], ['z', 'v'], ['i']]
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testImpossibleGadgetWithNoLoopPolicies(self):
        """This gadget is impossible, so we should duplicate."""
        g = self.gadgets.conext.copy()
        g.subpaths = [['u', 'v', 'z', 'w'], ['u', 'z', 'v', 'w']]
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testPredecessors(self):
        i = nx.DiGraph()
        i.add_edges_from([('t', 'y'), ('t', 'x'), ('y', 'u'), ('y', 'x'),
                          ('u', 'z'), ('x', 'z'), ('z', 'v'), ('v', 'w')])
        f = nx.DiGraph()
        f.add_edges_from([('t', 'u'), ('y', 'z'), ('y', 'x'),
                          ('u', 'v'), ('x', 'v'), ('v', 'z'), ('z', 'w')])
        g = flip.Graph(i, f, 'w', srcs=['u', 't'],
                       subpaths=[['v', 'z'], ['z', 'v']])
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testECMPWithAmbiguousGreenNodes(self):
        i = nx.DiGraph()
        i.add_path(['s1', 'b', 'c', 'd', 'z'])
        i.add_path(['s2', 'b', 'f', 'z'])
        f = nx.DiGraph()
        f.add_path(['s1', 'b', 'd', 'c', 'z'])
        f.add_path(['s1', 'b', 'e', 'z'])
        f.add_path(['s2', 'f', 'b'])
        g = flip.Graph(i, f, 'z', srcs=['s1', 's2'], subpaths=[
            ['b', 'f'], ['c', 'd'], ['d', 'c'], ['b', 'e']])
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testDangerousPaths(self):
        # Here node b is cyan-cyan, and a->b->z is a dangerous path
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'c', 'z'])
        i.add_path(['e', 'z'])
        f = nx.DiGraph()
        f.add_path(['s1', 'e', 'b', 'z'])
        g = flip.Graph(i, f, dst='z', srcs=['s1'], subpaths=[
            ['a', 'b', 'c'], ['e', 'b', 'z']])
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testDangerousPathsBlackHole(self):
        # Here node b is cyan-cyan, and a->b->z is a dangerous path
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'b', 'c', 'z'])
        f = nx.DiGraph()
        f.add_path(['s1', 'e', 'b', 'z'])
        g = flip.Graph(i, f, dst='z', srcs=['s1'], subpaths=[
            ['a', 'b', 'c'], ['e', 'b', 'z']])
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())
       
    def testImpossiblePolicies(self):
        g = self.gadgets.impossible_policies.copy()
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testYellowCyans(self):
        i = nx.DiGraph()
        i.add_path(['a', 'b', 'c', 'd', 'e', 'f'])
        i.add_path(['a1', 'j', 'f'])
        for new_node in 'ghijkl':
            i.add_edge(new_node, 'f')
        f = nx.DiGraph()
        f.add_path(['a', 'b', 'c', 'd', 'e', 'f'])
        f.add_path(['a', 'g', 'h', 'i', 'e', 'f'])
        f.add_path(['a', 'g', 'j', 'k', 'l', 'f'])
        f.add_path(['a1', 'a'])
        g = flip.Graph(i, f, dst='f', srcs=['a', 'a1'], subpaths=[
            ['c', 'd', 'e'], ['i', 'e', 'f'], ['j', 'k', 'l'],
            ['a1', 'j', 'f'], ['a1', 'a', 'g']])
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

    def testDisputeWheelWithNoPolicies(self):
        i = nx.DiGraph()
        i.add_path(['s1', 'a', 'z'])
        i.add_path(['b', 'c', 'z'])
        i.add_path(['s2', 'c', 'z'])
        i.add_path(['d', 'a', 'z'])
        f = nx.DiGraph()
        f.add_path(['s1', 'b', 'z'])
        f.add_path(['a', 'b', 'z'])
        f.add_path(['s2', 'd', 'z'])
        f.add_path(['c', 'd', 'z'])
        g = flip.Graph(i, f, dst='z', srcs=['s1', 's2'], subpaths=[
            ['s1'], ['s2']])
        for dup in (False, True):
            m = flip.compute_sequence(graph=g, duplicate=dup)
            self.assertTrue(m.verify())

if __name__ == '__main__':
    unittest.main()
