Repository with the code for the FLIP algorithm presented in "S. Vissicchio, L. Cittadini. FLIP the (Flow) Table: Fast LIghtweight Policy-preserving SDN Updates. In Proc. INFOCOM, 2016."

# Build and start the FLIP Virtual Machine

To ensure that the dependencies of our code are correctly set, we provide a Vagrantfile to build a Virtual Machine where our FLIP implementation runs correctly.

As a first step, ensure that [Vagrant](https://www.vagrantup.com) and [VirtualBox](https://www.virtualbox.org) are installed on your machine.
Then, clone this git repository and just instruct Vagrant to build the FLIP Virtual Machine.
Doing so on a command line would amount to running the following few commands.

    $ git clone git@gitlab.com:s.vissicchio/flip-vm.git
    $ cd flip-vm
    $ vagrant up

After the last command is successfully terminated, your FLIP Virtual Machine should be provisioned and running.
You can log in it from a command line by simply typing:

    $ vagrant ssh

# Check our FLIP implementation

Once inside the FLIP Virtual Machine, you will see a single folder named flip-code in the home directory of the vagrant user.
You can run our implementation of the FLIP algorithm from inside that directory.

For example, execute the following two commands for running the unit tests we used to check the correctness of our implementation:

    vagrant@debian-9:~$ cd flip-code
    vagrant@debian-9:~/flip-code$ python flip_test.py

The above commands should show evidence of our FLIP implementation, contained in the flip.py file, successfully passing our 40+ unit tests.

**NOTE**: Our FLIP code relies on gurobipy to use [Gurobi](https://www.gurobi.com) as optimization problem solver. The Vagrantfile instructs Vagrant to install gurobipy using pip, which ensures a default license to Gurobi at the time of writing. Such a default license is described as follows on the [Gurobi Web site](https://www.gurobi.com/documentation/9.1/quickstart_mac/cs_using_pip_to_install_gr.html):

> Our pip package includes a limited license that allows you to solve small optimization problems. If are an academic user, you can obtain a free, unlimited academic license from our website.

**NOTE**: An implication of the above note on the Gurobi license installed by default by Vagrant is that you may eventually need a different license to run your experiments -- e.g., when the default license from the pip package expires, or in order to run  experiments on larger topologies than the ones configured in our unit tests.  

# Run FLIP for your own experiments

Documentation on how to run our FLIP implementation from another Python script, we advise that you **check the EndToEndTestCase class inside the flip_test.py**: we believe that the code in this class is quite self-explanatory.
